# Steap

> [科技计划项目电子辅助验收及评估平台](https://gitee.com/Zychaowill/steap/tree/master/)

## 1. 项目分支

### 1.1 project branch

- master branch: Has finished.

- dev branch: Change Steap project to maven project.

### 1.2 code refactor（代码重构版本）

- master branch: initial commit.

- [dev branch](https://gitee.com/Zychaowill/Steapx): maven pom project and realize project state management based on state design pattern.
  - steapx
    - steapx-common
    - steapx-service
    - steapx-rest
    - steapx-portal

## 2. 项目介绍

> 科技计划项目电子辅助验收及评估平台是行政部门为规范科技项目管理、提高管理质量和工作效率而实施的一项应用研究。

### 2.1 项目采用技术

开发该项目主要采用的技术有：

- 前端: Bootstrap + jQuery + H5 + CSS3

- 后端: Spring + Struts2 + MyBatis

- 数据库: MySQL 5.6.29

- 其他技术: FineReport, Word2PDF, ExcelUtil

### 2.2 项目包结构

dev branch项目包结构：

- action: Struts2 Action

- bean: 数据库表Entity以及部分Bean Wrapper

- common: 工具包

- db/mapper: 数据访问层

- db/service: 业务层

### 2.3 项目主要功能

该项目主要由以下几个部分组成：

- 接收验收资料
  - 登记验收资料
  - 确定验收项目负责部门
  - 领取验收项目资料
  - 审查验收项目资料
  - 企业现场考察

- 验收会前准备
  - 待验收项目管理
  - 验收会议管理
  - 验收资料编制
  - 文档的固化管理
  - 编制会议议程（会议模板管理）
  - 通知相关人员

- 验收项目
  - 专家界面
    - 专家签到
    - 专家廉洁、回避承诺书签字
    - 查看项目验收资料
    - 填写专家项目验收个人意见，专家组长填写项目终审意见
  - 项目验收主持人
    - 打印专家签到表
    - 记录组长
    - 查看专家项目验收意见（打印）

- 验收后管理
  - 已验收项目查看
  - 验收项目资料完善
  - 小意见修改完成审核
  - 验收证书发放

- 统计报表
  - 提供对所验收项目按领域、年度进行统计分析，并能导出打印

- 系统管理
  - 用户管理
  - 角色管理
  - 菜单管理
  - 部门管理
  - 字典管理
  - 日志管理
  - 专家库管理
    - 实现专家信息的增加、删除、修改和查询
    - 专家签名管理
  - 评审意见管理（专家项目意见录入错误，可以修改状态，重新录入）
  - 项目综合查询

### 2.4 项目截图

[系统截图](https://github.com/Zychaowill/ImgStore/blob/master/Steap/)
