/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50629
 Source Host           : localhost
 Source Database       : steap

 Target Server Version : 50629
 File Encoding         : utf-8

 Date: 01/04/2018 00:05:51 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `complete_material`
-- ----------------------------
DROP TABLE IF EXISTS `complete_material`;
CREATE TABLE `complete_material` (
  `completeId` varchar(40) NOT NULL COMMENT '完善资料编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `fileId` int(11) NOT NULL,
  PRIMARY KEY (`completeId`),
  KEY `FK_Reference_27` (`projectId`),
  KEY `FK_Reference_30` (`fileId`),
  CONSTRAINT `FK_Reference_27` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`),
  CONSTRAINT `FK_Reference_30` FOREIGN KEY (`fileId`) REFERENCES `t_files_index` (`fileId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='完善资料表\r\n';

-- ----------------------------
--  Records of `complete_material`
-- ----------------------------
BEGIN;
INSERT INTO `complete_material` VALUES ('0f220f44-c170-401e-a668-4e2ddf24b5d9', '123', '42'), ('19c8071f-7be1-4b1f-a33d-12e9bb2ce159', 'P1002', '141'), ('3368a294-cd4e-4028-a77a-5065293cc485', '20170318测试项目3', '131'), ('74873e36-5902-4876-8098-9129c8b82446', '20170318测试项目', '140'), ('78fa7550-ccb4-47c9-b569-893b01e00963', '20170316测试项目', '72'), ('baf2ca0b-2cea-48a0-9d7d-450640209a0f', '20170316测试项目', '73'), ('cb7438b1-6227-402f-816f-357f84e9aacb', '20170318测试项目3', '129');
COMMIT;

-- ----------------------------
--  Table structure for `complete_verify`
-- ----------------------------
DROP TABLE IF EXISTS `complete_verify`;
CREATE TABLE `complete_verify` (
  `verifyId` int(11) NOT NULL COMMENT '项目审核编号',
  `completeId` varchar(40) NOT NULL,
  PRIMARY KEY (`verifyId`,`completeId`),
  KEY `FK_Reference_verifyId` (`verifyId`),
  KEY `FK_Reference_materialId` (`completeId`),
  CONSTRAINT `FK_Reference_completeId` FOREIGN KEY (`completeId`) REFERENCES `complete_material` (`completeId`) ON DELETE CASCADE,
  CONSTRAINT `FK_Reference_verifyId` FOREIGN KEY (`verifyId`) REFERENCES `project_verify` (`verifyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='审核完善资料表';

-- ----------------------------
--  Records of `complete_verify`
-- ----------------------------
BEGIN;
INSERT INTO `complete_verify` VALUES ('11', '78fa7550-ccb4-47c9-b569-893b01e00963'), ('11', 'baf2ca0b-2cea-48a0-9d7d-450640209a0f'), ('13', 'cb7438b1-6227-402f-816f-357f84e9aacb'), ('14', '3368a294-cd4e-4028-a77a-5065293cc485'), ('16', '74873e36-5902-4876-8098-9129c8b82446');
COMMIT;

-- ----------------------------
--  Table structure for `dic_system_dictionary`
-- ----------------------------
DROP TABLE IF EXISTS `dic_system_dictionary`;
CREATE TABLE `dic_system_dictionary` (
  `dictionaryOptionId` varchar(40) NOT NULL,
  `dictionaryOptionName` varchar(50) DEFAULT NULL,
  `upDictionaryOptionId` varchar(40) DEFAULT NULL,
  `isUse` bit(1) DEFAULT b'1',
  `description` varchar(200) DEFAULT NULL,
  `sortNo` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `englishName` varchar(100) DEFAULT NULL,
  `dataType` varchar(100) DEFAULT NULL,
  `remark1` varchar(50) DEFAULT NULL,
  `remark2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dictionaryOptionId`),
  KEY `FK_DIC` (`upDictionaryOptionId`),
  CONSTRAINT `FK_DIC` FOREIGN KEY (`upDictionaryOptionId`) REFERENCES `dic_system_dictionary` (`dictionaryOptionId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统字典表';

-- ----------------------------
--  Records of `dic_system_dictionary`
-- ----------------------------
BEGIN;
INSERT INTO `dic_system_dictionary` VALUES ('1', '字典列表', null, b'1', '字典列表', '0', '0', 'dictionaryList', '字符型', null, null), ('100', '项目状态', '1', b'1', '项目状态', '0', '0', 'projectStatus', '字符型', null, null), ('100001', '已登记', '100', b'1', '已登记', '0', '0', 'recorded', '字符型', null, null), ('100002', '已确定验收负责部门', '100', b'1', '已确定验收负责部门', '0', '0', 'charged', '字符型', null, null), ('100003', '已领取验收项目资料', '100', b'1', '已领取验收项目资料', '0', '0', 'recieved', '字符型', null, null), ('100004', '已审查', '100', b'1', '已审查', '0', '0', 'verified', '字符型', null, null), ('100005', '已现场考察', '100', b'1', '已企业现场考场', '0', '0', '', '字符型', '', null), ('100006', '已创建项目会议', '100', b'1', '已创建项目会议', '0', '0', '', '字符型', '', null), ('100007', '已评审', '100', b'1', '已评审', '0', '0', null, '字符型', null, null), ('100008', '已评估', '100', b'1', '已评估', '0', '0', null, '字符型', null, null), ('100009', '已通过审核', '100', b'1', '已通过审核', '0', '0', '', '字符型', '', ''), ('100010', '已发放证书', '100', b'1', '已发放证书', '0', '0', 'issue_certificate', '字符型', null, null), ('200', '会议状态', '1', b'1', '会议状态', '0', '0', 'meetingStatus', '字符型', null, null), ('200001', '准备中', '200', b'1', '准备中', '0', '0', 'preparing', '字符型', null, null), ('200002', '召开中', '200', b'1', '召开中', '0', '0', 'holding', '字符型', null, null), ('200003', '已完成', '200', b'1', '已完成', '0', '0', 'finished', '字符型', null, null), ('300', '文件类型', '1', b'1', '文件类型', '0', '0', 'fileType', '字符型', null, null), ('300001', '验收意见初稿', '300', b'1', '验收意见初稿', '0', '0', null, '字符型', null, null), ('300002', '科技项目验收申请书', '300', b'1', '科技项目验收申请书', '0', '0', null, '字符型', null, null), ('300003', '承诺书', '300', b'1', '承诺书', '0', '0', null, '字符型', null, null), ('300004', '计划任务书', '300', b'1', '计划任务书', '0', '0', null, '字符型', null, null), ('300005', '项目实施工作总结报告', '300', b'1', '项目实施工作总结报告', '0', '0', null, '字符型', null, null), ('300006', '项目实施技术总结报告', '300', b'1', '项目实施技术总结报告', '0', '0', null, '字符型', null, null), ('300007', '项目立项后相关证明材料', '300', b'1', '项目立项后相关证明材料', '0', '0', null, '字符型', null, null), ('300008', '科技项目验收财务资料一套', '300', b'1', '科技项目验收财务资料一套', '0', '0', null, '字符型', null, null), ('400', '图片类型', '1', b'1', '图片类型', '0', '0', 'pictureType', '字符型', null, null), ('400001', '考察照片', '400', b'1', '考察照片', '0', '0', null, '字符型', null, null), ('400002', '廉洁承诺书', '400', b'1', '廉洁承诺书', '0', '0', null, '字符型', null, null), ('400003', '真实性承诺书', '400', b'1', '真实性承诺书', '0', '0', null, '字符型', null, null), ('400004', '审计报告', '400', b'1', '审计报告图片', '0', '0', null, '字符型', null, null), ('500', '机构', '1', b'1', '机构', '0', '0', 'institution', '字符型', null, null), ('500001', '专项管理机构业务处室', '500', b'1', '专项管理机构业务处室', '0', '0', null, '字符型', null, null), ('500002', '局纪检监察领导', '500', b'1', '局纪检监察领导', '0', '0', null, '字符型', null, null), ('600', '领域', '1', b'1', '领域', '0', '0', 'domin', '字符型', '', null), ('600001', '能源与节能环保领域', '600', b'1', '能源与节能环保领域', '0', '0', null, '字符型', null, null), ('600002', '现代服务业领域', '600', b'1', '现代服务业领域', '0', '0', null, '字符型', null, null), ('600003', '交通运输领域', '600', b'1', '交通运输领域', '0', '0', null, '字符型', null, null), ('600004', '信息领域', '600', b'1', '信息领域', '0', '0', null, '字符型', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `enterprise_inspect`
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_inspect`;
CREATE TABLE `enterprise_inspect` (
  `inspectId` varchar(10) NOT NULL COMMENT '考察编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `inspectPerson` varchar(100) NOT NULL,
  `inspectDate` date DEFAULT NULL,
  `inspectResult` longtext NOT NULL,
  `isAudit` bit(1) NOT NULL DEFAULT b'0',
  `inspectStatus` varchar(10) DEFAULT NULL COMMENT '考察状态',
  `enterpriseReceiver` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`inspectId`),
  KEY `FK_Reference_3` (`projectId`),
  CONSTRAINT `FK_Reference_3` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业考察表';

-- ----------------------------
--  Records of `enterprise_inspect`
-- ----------------------------
BEGIN;
INSERT INTO `enterprise_inspect` VALUES ('EI10000001', 'P1000032', 'U100003', '2016-10-09', '通过', b'1', '已保存', null), ('EI10000002', 'P1001', 'U100003', '2016-10-31', '达到预期目标', b'1', '已提交', null), ('EI10000003', 'P1002', 'U100003', '2016-10-31', '达到要求', b'1', '已提交', null), ('EI10000004', 'ty20161207', '啊，吧，你，', '2016-12-07', 'ok', b'0', '已提交', null), ('EI10000005', '2311123', '  张亚超', '2017-02-08', '  张亚超', b'1', '已提交', '  张亚超'), ('EI10000006', 'P1003', '  张亚超', '2017-02-08', '  张亚超', b'0', '已提交', '  张亚超'), ('EI10000007', '123', 'aaa,xsss', '2017-03-09', '更符合规划vb，\r\n从vbhnkjk，了\r\n出手的话此时的好处是多长', b'0', '已提交', 'bbb'), ('EI10000008', '456', 'fasf', '2017-03-07', 'fasfdsafdsfad', b'1', '已提交', 'fasdf'), ('EI10000009', '20170316测试项目', 'Zychaowill', '2017-03-16', '20170316测试项目', b'1', '已提交', 'Zychaowill'), ('EI10000010', '20170318测试项目', '张亚超', '2017-03-18', '20170318测试项目', b'1', '已提交', '张亚超'), ('EI10000011', '20170318测试项目3', 'hss', '2017-03-18', 'fasdfasfsdfasdf', b'0', '已提交', 'ffaf'), ('EI10000012', '20170318测试项目4', 'hss', '2017-03-18', 'fasfas', b'0', '已提交', 'faffasd'), ('EI10000013', '20170318测试项目2', '张亚超', '2017-06-14', '通过', b'0', '已提交', '张亚超');
COMMIT;

-- ----------------------------
--  Table structure for `evaluation_result`
-- ----------------------------
DROP TABLE IF EXISTS `evaluation_result`;
CREATE TABLE `evaluation_result` (
  `evaluateResultId` varchar(40) NOT NULL COMMENT '评审结果编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `specialistId` varchar(8) DEFAULT NULL,
  `evaluateResult` longtext NOT NULL COMMENT '评审意见',
  `remark` varchar(50) DEFAULT '0' COMMENT '备注',
  `checkDate` date NOT NULL,
  PRIMARY KEY (`evaluateResultId`),
  UNIQUE KEY `projectId` (`projectId`),
  UNIQUE KEY `projectId_2` (`projectId`),
  UNIQUE KEY `projectId_3` (`projectId`),
  KEY `FK_Reference_17` (`projectId`),
  KEY `specialistId` (`specialistId`),
  CONSTRAINT `FK_Reference_17` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`),
  CONSTRAINT `evaluation_result_ibfk_1` FOREIGN KEY (`specialistId`) REFERENCES `specialist_library` (`specialistId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评审结果表';

-- ----------------------------
--  Records of `evaluation_result`
-- ----------------------------
BEGIN;
INSERT INTO `evaluation_result` VALUES ('0438dd0e-bddc-11e6-b5f0-1e66a60e516e', 'P1000032', null, 'updateEvaluationResultupdateEvaluationResultupdateEvaluationResultupdateEvaluationResultupdateEvaluationResultupdateEvaluationResult', '1', '2016-12-09'), ('49703822-0968-11e7-a01f-a8baaa26c050', 'P1002', 'SL100001', '很不错的项目。<div>尤其是还采用了Zychaowill实现的功能。</div>', '1', '2017-03-15'), ('52d650ca-0ee7-11e7-98a8-ce4a06423956', '20170318测试项目3', 'SL100001', '<div><b>Struts 2 (FreeMarker、Velocity) &nbsp; SpringMVC 4</b></div><div><b>Spring 4</b></div><div><b>Mybatis 3 + Hibernate 4</b></div><div><b>MySQL、Oracle</b></div><div><b>nginx + tomcat/WebLogic &nbsp; &nbsp;jetty</b></div><div><b>memcached/redis、mongodb</b></div><div><b>WebService、Restful</b></div><div><b>数据库连接池 （阿里巴巴） Druid</b></div><div><b>项目安全框架 Shiro（可验证不同的url请求）</b></div><div><b>Lucene全站检索功能</b></div><div><b>设计模式、Internet基本协议（如TCP/IP、HTTP等）内容及相关应用</b></div><div><b>Bootstrap 3 UI框架进行前端界面设计</b></div><div><b>EasyUI框架进行后台界面设计</b></div><div><b>百度编辑器Ueditor</b></div><div><b>优化算法、算法与数据结构</b></div><div><b><br>fadfafdas分布式集群</b></div><div><br></div>', '1', '2017-03-22'), ('791c3668-eeb8-11e6-993c-f7eec40ae560', '2311123', null, 'FADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGA<br>', '1', '2017-02-09'), ('7d1cda06-eeb8-11e6-993c-f7eec40ae560', 'P1003', null, 'FADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGAFADFABLABG;ANDG;EDBGEDFSGSGAGA<br>EGAEGA<br>', '1', '2017-02-09'), ('9ba3e47c-436a-11e7-af9b-7a10e0366a71', '20170318测试项目', 'SL100001', '该项目良好，通过。', '1', '2017-05-28'), ('9f0b3d72-0576-11e7-98aa-076d0efed018', '456', null, '23333333333333333333333<br>', '1', '2017-03-10'), ('a7ecc21c-0576-11e7-98aa-076d0efed018', '123', null, 'sjhjshjshajhsajhjsahjsahsjhsjahjahjshajh', '1', '2017-03-10'), ('d770678e-0574-11e7-98aa-076d0efed018', 'ty20161207', null, 'end meeting<br>', '1', '2017-03-10'), ('f0c6748e-0967-11e7-a01f-a8baaa26c050', 'P1001', 'SL100001', '很不错的项目。<div>尤其是还采用了Zychaowill实现的功能。</div>', '1', '2017-03-15');
COMMIT;

-- ----------------------------
--  Table structure for `inspect_audit`
-- ----------------------------
DROP TABLE IF EXISTS `inspect_audit`;
CREATE TABLE `inspect_audit` (
  `auditId` varchar(40) NOT NULL COMMENT '审计编号',
  `inspectId` varchar(10) NOT NULL COMMENT '考察编号',
  `financeUnit` varchar(30) NOT NULL COMMENT '财务单位\r\n            ',
  `auditingPerson` varchar(30) NOT NULL COMMENT '审计人员',
  `auditDate` date NOT NULL COMMENT '审计日期',
  `enterpriseReceiver2` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`auditId`),
  KEY `FK_Reference_16` (`inspectId`),
  CONSTRAINT `FK_Reference_16` FOREIGN KEY (`inspectId`) REFERENCES `enterprise_inspect` (`inspectId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目审计表\r\n';

-- ----------------------------
--  Records of `inspect_audit`
-- ----------------------------
BEGIN;
INSERT INTO `inspect_audit` VALUES ('1fa4cbf4-0574-11e7-98aa-076d0efed018', 'EI10000008', 'fasdf', 'fasffasf', '2017-03-07', 'fsdfsa'), ('4a5d1a8a-0ba6-11e7-8f64-57e474948c03', 'EI10000010', '科技大学', '张亚超', '2017-03-18', '张亚超'), ('50a9a4ce-8dc7-11e6-8d77-469f4a9716a8', 'EI10000001', '科大', '张亚超', '2016-10-09', null), ('62c74ef2-9f35-11e6-966a-88719ec3f890', 'EI10000003', '财务局', '阿斯', '2016-10-31', null), ('77cc4386-edfd-11e6-9cc5-e9fa2be60257', 'EI10000005', '科技大学', '  张亚超', '2017-02-08', '  张亚超'), ('7f1c271a-0a35-11e7-a696-6431c54c2cf5', 'EI10000009', '20170316测试项目', '20170316测试项目', '2017-03-16', '20170316测试项目'), ('c79723ae-9f33-11e6-966a-88719ec3f890', 'EI10000002', '财务局', '阿斯', '2016-11-01', null);
COMMIT;

-- ----------------------------
--  Table structure for `institution_notify`
-- ----------------------------
DROP TABLE IF EXISTS `institution_notify`;
CREATE TABLE `institution_notify` (
  `notifyId` varchar(40) NOT NULL COMMENT '通知编号',
  `meetingId` varchar(7) NOT NULL COMMENT '会议编号',
  `institutionName` varchar(30) NOT NULL COMMENT '部门名称',
  `notifier` varchar(30) NOT NULL COMMENT '通知人',
  `notifyDate` date DEFAULT NULL COMMENT '通知日期',
  `notifyMethod` varchar(50) DEFAULT NULL,
  `contactPerson` varchar(30) NOT NULL,
  `notifyResult` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`notifyId`),
  KEY `FK_Reference_34` (`meetingId`),
  CONSTRAINT `FK_Reference_34` FOREIGN KEY (`meetingId`) REFERENCES `t_meeting_base_info` (`meetingId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构通知表';

-- ----------------------------
--  Records of `institution_notify`
-- ----------------------------
BEGIN;
INSERT INTO `institution_notify` VALUES ('027377be-9f36-11e6-966a-88719ec3f890', 'M100002', 'fgfsdfdsfd', 'U100001', '2016-11-03', '电话', 'fdss', '\n							fsdfdsfdsfd\n						\n						'), ('1c8a2f6c-9f36-11e6-966a-88719ec3f890', 'M100002', 'gfgs', 'U100004', '2016-10-10', '电话', 'fdsf', 'fdsfsfd'), ('5529675a-8dd1-11e6-8d77-469f4a9716a8', 'M100001', '专项管理机构业务处室', 'U100004', '2016-10-09', '电话', '张亚超', '通知成功'), ('5531a4d8-8dd1-11e6-8d77-469f4a9716a8', 'M100001', '专项管理机构业务处室', 'U100004', '2016-10-09', '电话', '张亚超', '通知成功'), ('5531a6b8-8dd1-11e6-8d77-469f4a9716a8', 'M100001', '局纪检监察领导', 'U100004', '2016-10-09', 'QQ', '张亚超', '通知成功'), ('78b27a66-0574-11e7-98aa-076d0efed018', 'M100006', '专项管理机构业务处室', 'U100003', '2017-03-14', '微信', '123456', '123456'), ('79bf521a-0ba7-11e7-8f64-57e474948c03', 'M100008', '专项管理机构业务处室', 'U100004', '2017-03-18', '电话/微信', '张亚超', '成功'), ('79c0322a-0ba7-11e7-8f64-57e474948c03', 'M100008', '局纪检监察领导', 'U100004', '2017-03-18', '电话/微信/短信', '张亚超', '成功'), ('79c0c8d4-0ba7-11e7-8f64-57e474948c03', 'M100008', '财务审查监', 'U100004', '2017-03-18', '电话/QQ/微信/短信', '张亚超', '成功'), ('950144c2-0574-11e7-98aa-076d0efed018', 'M100005', '局纪检监察领导', 'U100004', '2017-02-28', '微信', 'fds', 'fd'), ('b75c006e-0a35-11e7-a696-6431c54c2cf5', 'M100007', '20170316测试项目', 'U100004', '2017-03-16', '电话', '20170316测试项目', '20170316测试项目'), ('b75ce164-0a35-11e7-a696-6431c54c2cf5', 'M100007', '专项管理机构业务处室', 'U100004', '2017-03-16', '电话', '20170316测试项目', 'email'), ('c600837c-bc8f-11e6-b226-d47ef86308fd', 'M100003', '专项管理机构业务处室', 'U100004', '2016-12-07', '电话', '张亚超', '\n							成功\n						\n						'), ('c9b28b88-edfd-11e6-9cc5-e9fa2be60257', 'M100004', '专项管理机构业务处室', 'U100004', '2017-02-08', '电话', '  张亚超', '成功'), ('f25e9474-0bb6-11e7-8f64-57e474948c03', 'M100009', '专项管理机构业务处室', 'U100005', '2017-03-18', '电话/微信', '张亚超', '成功'), ('f25f364a-0bb6-11e7-8f64-57e474948c03', 'M100009', '纪检委', 'U100005', '2017-03-16', '电话/微信', '张亚超', '成功'), ('f25fcd4e-0bb6-11e7-8f64-57e474948c03', 'M100009', '局纪检监察领导', 'U100005', '2017-03-18', '电话/微信', '张亚超', '成功');
COMMIT;

-- ----------------------------
--  Table structure for `issue_certificate`
-- ----------------------------
DROP TABLE IF EXISTS `issue_certificate`;
CREATE TABLE `issue_certificate` (
  `issueId` varchar(40) NOT NULL COMMENT '发放编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `issuePerson` varchar(30) NOT NULL COMMENT '发放人',
  `issueDate` date DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `holder` varchar(30) DEFAULT NULL,
  `holderPhone` varchar(16) DEFAULT NULL,
  `holdDate` date DEFAULT NULL,
  PRIMARY KEY (`issueId`),
  KEY `FK_Reference_20` (`projectId`),
  CONSTRAINT `FK_Reference_20` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='证书发放表';

-- ----------------------------
--  Records of `issue_certificate`
-- ----------------------------
BEGIN;
INSERT INTO `issue_certificate` VALUES ('81d1e7f0-436b-11e7-af9b-7a10e0366a71', '20170318测试项目', 'U100003', '2017-05-28', '', '丁英杰', '18435156270', '2017-05-28'), ('e25f8f4c-0576-11e7-98aa-076d0efed018', '456', 'U100003', '2017-03-06', '', 'fadaf', 'fsadffsdaf', '2017-03-09'), ('e8c5c948-e8f5-11e6-8d7f-d25221ae0921', 'P1000032', 'U100003', '2017-02-09', '', '张亚超', '18435155316', '2017-02-11'), ('fd641c0e-0576-11e7-98aa-076d0efed018', '123', 'U100003', '2017-03-17', '', '阿斯蒂芬', '13245647656', '2017-03-18');
COMMIT;

-- ----------------------------
--  Table structure for `project_accept`
-- ----------------------------
DROP TABLE IF EXISTS `project_accept`;
CREATE TABLE `project_accept` (
  `acceptId` varchar(40) NOT NULL COMMENT '项目接收编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `recordDate` date NOT NULL COMMENT '登记日期',
  `recordPerson` varchar(30) NOT NULL COMMENT '登记人',
  `chargeUnit` varchar(30) DEFAULT NULL COMMENT '负责部门',
  `chargePerson` varchar(30) DEFAULT NULL COMMENT '负责人',
  `distributionDate` date DEFAULT NULL COMMENT '分配时间',
  `receiveDate` date DEFAULT NULL COMMENT '领取时间',
  `receivePerson` varchar(30) DEFAULT NULL COMMENT '领取人',
  `receiveResult` longtext,
  `remark1` varchar(50) DEFAULT NULL COMMENT '备注1',
  `remark2` varchar(50) DEFAULT NULL COMMENT '备注2',
  `remark3` varchar(50) DEFAULT NULL COMMENT '备注3',
  PRIMARY KEY (`acceptId`),
  KEY `FK_Reference_5` (`projectId`),
  CONSTRAINT `FK_Reference_5` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目接收表';

-- ----------------------------
--  Records of `project_accept`
-- ----------------------------
BEGIN;
INSERT INTO `project_accept` VALUES ('0cbe3650-0bb0-11e7-8f64-57e474948c03', '20170318测试项目4', '2017-03-18', 'U100002', '主任办公室', 'U100003 ', '2017-03-18', '2017-03-18', 'U100003', 'dfaf', null, null, null), ('0d9505c2-9f45-11e6-966a-88719ec3f890', 'P101', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2016-12-08', 'U100003', '、立刻就会股份', null, null, null), ('11b7286c-9f57-11e6-966a-88719ec3f890', 'asdads', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-12-08', null, null, null, null, null, null), ('13b06cde-9f32-11e6-966a-88719ec3f890', 'P1001', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2016-09-27', 'U100003', '全部领取', null, null, null), ('16135a0e-0a35-11e7-a696-6431c54c2cf5', '20170316测试项目', '2017-03-16', 'U100002', '主任办公室', 'U100003 ', '2017-03-16', '2017-03-16', 'U100003', 'OK', null, null, null), ('281fe42e-9f46-11e6-966a-88719ec3f890', 'P100036', '2016-10-31', 'U100002', null, null, null, null, null, null, null, null, null), ('4e4e499c-9f5a-11e6-966a-88719ec3f890', 'dasdadasda', '2016-10-31', 'U100003', '主任办公室', 'U100003 ', '2016-12-08', '2016-11-28', 'U100003', '撒旦法', null, null, null), ('5d287176-9f56-11e6-966a-88719ec3f890', 'eqweq', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2017-05-27', 'U100003', 'sdfas', null, null, null), ('603e8582-8dc5-11e6-8d77-469f4a9716a8', 'P1000032', '2016-10-09', 'U100002', '主任办公室', 'U100003', '2016-10-09', '2016-10-09', 'U100003', 'OK', null, null, null), ('6d3566ea-bc8f-11e6-b226-d47ef86308fd', 'ty20161207', '2016-12-07', 'U100002', '主任办公室', 'U100003 ', '2016-12-07', '2016-12-07', 'U100003', 'ok', null, null, null), ('6efece66-0baa-11e7-8f64-57e474948c03', '20170318测试项目3', '2017-03-18', 'U100002', '主任办公室', 'U100003 ', '2017-03-18', '2017-03-18', 'U100003', 'asfsd', null, null, null), ('74a88910-9f58-11e6-966a-88719ec3f890', 'dasda', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2017-06-14', 'U100003', ' 已全部领取', null, null, null), ('75697220-9f4d-11e6-966a-88719ec3f890', 'P1000011', '2016-10-31', 'U100002', '主任办公室', 'U100002 ', '2016-10-31', null, null, null, null, null, null), ('84783c92-0573-11e7-98aa-076d0efed018', '123', '2017-03-07', 'U100002', '主任办公室', 'U100003 ', '2017-03-10', '2017-03-07', 'U100003', '法国红酒看来', null, null, null), ('85e76adc-9f32-11e6-966a-88719ec3f890', 'P1002', '2016-10-12', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2016-10-01', 'U100003', '全部领取', null, null, null), ('aeb44144-0ba5-11e7-8f64-57e474948c03', '20170318测试项目', '2017-03-18', 'U100002', '主任办公室', 'U100003 ', '2017-03-18', '2017-03-18', 'U100003', 'OK', null, null, null), ('af856908-0ba6-11e7-8f64-57e474948c03', '20170318测试项目2', '2017-03-18', 'U100002', '主任办公室', 'U100003 ', '2017-03-18', '2017-03-18', 'U100003', 'asfasdf', null, null, null), ('b3b417e4-9f32-11e6-966a-88719ec3f890', 'P1003', '2016-10-06', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', '2017-02-08', 'U100003', 'OK', null, null, null), ('ba60ebb0-0573-11e7-98aa-076d0efed018', '456', '2017-03-07', 'U100002', '主任办公室', 'U100003 ', '2017-03-10', '2017-03-08', 'U100003', 'fasdfsad', null, null, null), ('be5fcece-9f53-11e6-966a-88719ec3f890', '2311123', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-12-08', '2017-02-08', 'U100003', 'OK', null, null, null), ('ca2cddb4-9f4e-11e6-966a-88719ec3f890', 'P100039', '2016-10-31', 'U100002', null, null, null, null, null, null, null, null, null), ('f6108b70-9f48-11e6-966a-88719ec3f890', 'P100040', '2016-10-31', 'U100002', '主任办公室', 'U100003 ', '2016-10-31', null, null, null, null, null, null), ('f74f02ba-9f47-11e6-966a-88719ec3f890', 'P100037', '2016-10-31', 'U100002', null, null, null, null, null, null, null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `project_censor`
-- ----------------------------
DROP TABLE IF EXISTS `project_censor`;
CREATE TABLE `project_censor` (
  `censorId` varchar(40) NOT NULL COMMENT '审查Id',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `censorDate` date NOT NULL COMMENT '审查时间',
  `censorPerson` varchar(30) NOT NULL COMMENT '审查人',
  `censorResult` longtext NOT NULL COMMENT '审查意见',
  PRIMARY KEY (`censorId`),
  KEY `FK_Reference_2` (`projectId`),
  CONSTRAINT `FK_Reference_2` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目审查表';

-- ----------------------------
--  Records of `project_censor`
-- ----------------------------
BEGIN;
INSERT INTO `project_censor` VALUES ('1aba7dbc-0ba7-11e7-8f64-57e474948c03', '20170318测试项目2', '2017-03-18', 'U100003', 'fasfafdsfasdfasdfdsafasdf'), ('265baaa2-0bb0-11e7-8f64-57e474948c03', '20170318测试项目4', '2017-03-18', 'U100003', 'asfsadfasdfads'), ('2bb17cd6-9f34-11e6-966a-88719ec3f890', 'P1002', '2016-10-01', 'U100003', '符合要求'), ('2e9e734c-0a35-11e7-a696-6431c54c2cf5', '20170316测试项目', '2017-03-16', 'U100003', 'fadf'), ('3cefc7c4-bd10-11e6-937f-baf6a34c9506', 'P101', '2016-11-28', 'U100003', '吧'), ('4529f7c0-edfd-11e6-9cc5-e9fa2be60257', '2311123', '2017-02-08', 'U100003', 'OK'), ('45d10722-9f33-11e6-966a-88719ec3f890', 'P1001', '2016-10-01', 'U100003', '符合需求'), ('59a46ff0-edfd-11e6-9cc5-e9fa2be60257', 'P1003', '2017-02-08', 'U100003', 'OK'), ('77f31b6e-bd10-11e6-937f-baf6a34c9506', 'dasdadasda', '2016-11-27', 'U100003', '结婚过分的'), ('84647d50-0baa-11e7-8f64-57e474948c03', '20170318测试项目3', '2017-03-18', 'U100003', 'fasdfsf'), ('86ab13cc-bc8f-11e6-b226-d47ef86308fd', 'ty20161207', '2016-12-07', 'U100003', 'ok'), ('ccfb7dd0-0573-11e7-98aa-076d0efed018', '123', '2017-03-07', 'U100003', '发广告哈哈尽快解答'), ('d28e8b1a-0ba5-11e7-8f64-57e474948c03', '20170318测试项目', '2017-03-18', 'U100003', 'ok'), ('d9fa1580-42ba-11e7-af9b-7a10e0366a71', 'eqweq', '2017-05-27', 'U100003', 'das'), ('f0b6a32e-8dc5-11e6-8d77-469f4a9716a8', 'P1000032', '2016-10-09', 'U100003', '通过'), ('f7f7d6c8-0573-11e7-98aa-076d0efed018', '456', '2017-03-02', 'U100003', 'ffas');
COMMIT;

-- ----------------------------
--  Table structure for `project_material`
-- ----------------------------
DROP TABLE IF EXISTS `project_material`;
CREATE TABLE `project_material` (
  `materialId` varchar(40) NOT NULL,
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `fileId` int(11) NOT NULL,
  PRIMARY KEY (`materialId`),
  UNIQUE KEY `fileId` (`fileId`),
  KEY `FK_Reference_18` (`projectId`),
  KEY `FK_Reference_29` (`fileId`),
  CONSTRAINT `FK_FILE` FOREIGN KEY (`fileId`) REFERENCES `t_files_index` (`fileId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_18` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目资料表\r\n';

-- ----------------------------
--  Records of `project_material`
-- ----------------------------
BEGIN;
INSERT INTO `project_material` VALUES ('059f8250-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '142'), ('07b4561a-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '143'), ('09b77032-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '144'), ('0bbb6f18-edfe-11e6-9cc5-e9fa2be60257', '2311123', '8'), ('0bfbe620-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '145'), ('0e1604cc-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '146'), ('0e60211e-edfe-11e6-9cc5-e9fa2be60257', '2311123', '9'), ('10129060-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '147'), ('1130b5ac-edfe-11e6-9cc5-e9fa2be60257', '2311123', '10'), ('122b2a56-50cc-11e7-98fb-4f8bcc5d6d0b', '20170316测试项目', '148'), ('13f90424-edfe-11e6-9cc5-e9fa2be60257', '2311123', '11'), ('1717d626-edfe-11e6-9cc5-e9fa2be60257', '2311123', '12'), ('19652dca-edfe-11e6-9cc5-e9fa2be60257', '2311123', '13'), ('1bcab58a-edfe-11e6-9cc5-e9fa2be60257', '2311123', '14'), ('230c6438-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '15'), ('259830e2-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '16'), ('2641a0ec-0bb7-11e7-8f64-57e474948c03', '20170318测试项目4', '93'), ('27b8a94c-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '17'), ('29d18a6e-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '18'), ('2c0a37c0-0575-11e7-98aa-076d0efed018', '123', '39'), ('2c27710c-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '19'), ('2e86ae4a-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '20'), ('30c3567c-edfe-11e6-9cc5-e9fa2be60257', 'P1003', '21'), ('36813b36-0575-11e7-98aa-076d0efed018', '123', '40'), ('3afa3142-0ee7-11e7-98a8-ce4a06423956', '20170318测试项目3', '126'), ('3ca3ce20-0575-11e7-98aa-076d0efed018', '123', '41'), ('523772f0-e85b-11e6-ab31-a9497366a301', 'ty20161207', '1'), ('5bb6fbac-e85b-11e6-ab31-a9497366a301', 'ty20161207', '2'), ('6102fab6-e85b-11e6-ab31-a9497366a301', 'ty20161207', '3'), ('61bca942-15d4-11e7-9d06-31d9add62c55', '20170318测试项目3', '132'), ('66b830fc-0bb5-11e7-8f64-57e474948c03', '20170318测试项目4', '82'), ('6bd10a12-0bb7-11e7-8f64-57e474948c03', '20170318测试项目4', '95'), ('6e0129e0-e85b-11e6-ab31-a9497366a301', 'ty20161207', '4'), ('785ef764-e85b-11e6-ab31-a9497366a301', 'ty20161207', '5'), ('7a1d855a-0ba3-11e7-8f64-57e474948c03', '20170316测试项目', '76'), ('817029b8-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '133'), ('81979c00-e85b-11e6-ab31-a9497366a301', 'ty20161207', '6'), ('84f3d37a-0929-11e7-a01f-a8baaa26c050', 'P1001', '43'), ('863456e6-0574-11e7-98aa-076d0efed018', '456', '22'), ('8b9e8416-e85b-11e6-ab31-a9497366a301', 'ty20161207', '7'), ('8f0e5c98-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '134'), ('8fbac79a-0574-11e7-98aa-076d0efed018', '123', '23'), ('919f309a-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '135'), ('93523864-0ba7-11e7-8f64-57e474948c03', '20170318测试项目', '77'), ('941bfb5a-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '136'), ('971d605a-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '137'), ('972020a2-0574-11e7-98aa-076d0efed018', '456', '24'), ('9968c622-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '97'), ('99f61a88-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '138'), ('9b4cd3ee-0bb1-11e7-8f64-57e474948c03', '20170318测试项目4', '78'), ('9d1c7566-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '98'), ('9d56964a-0574-11e7-98aa-076d0efed018', '456', '25'), ('9e7fa2cc-4369-11e7-af9b-7a10e0366a71', '20170318测试项目', '139'), ('a02f291a-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '99'), ('a24f2b0e-0929-11e7-a01f-a8baaa26c050', 'P1002', '44'), ('a2a79d6a-0574-11e7-98aa-076d0efed018', '456', '26'), ('a36b8718-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '100'), ('a736f06a-0574-11e7-98aa-076d0efed018', '456', '27'), ('a99992ec-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '101'), ('abe83386-0929-11e7-a01f-a8baaa26c050', 'P1001', '45'), ('ac6039de-0574-11e7-98aa-076d0efed018', '456', '28'), ('ad5e490e-0bb7-11e7-8f64-57e474948c03', '20170318测试项目3', '102'), ('b12151e2-0574-11e7-98aa-076d0efed018', '456', '30'), ('b2cf781c-0929-11e7-a01f-a8baaa26c050', 'P1001', '46'), ('b5da114c-0574-11e7-98aa-076d0efed018', '456', '31'), ('b711dbf4-0929-11e7-a01f-a8baaa26c050', 'P1001', '47'), ('b9d11980-0574-11e7-98aa-076d0efed018', '123', '32'), ('bacef074-0929-11e7-a01f-a8baaa26c050', 'P1001', '48'), ('c0d959be-0929-11e7-a01f-a8baaa26c050', 'P1001', '49'), ('c0fc493a-0bb6-11e7-8f64-57e474948c03', '20170318测试项目4', '85'), ('c4cc4f0e-0929-11e7-a01f-a8baaa26c050', 'P1001', '50'), ('c8951f3a-0929-11e7-a01f-a8baaa26c050', 'P1001', '51'), ('c8ed1a20-0bb6-11e7-8f64-57e474948c03', '20170318测试项目4', '87'), ('ce8226e0-0929-11e7-a01f-a8baaa26c050', 'P1002', '52'), ('d0f3e96a-0bb6-11e7-8f64-57e474948c03', '20170318测试项目4', '88'), ('d23bce94-0929-11e7-a01f-a8baaa26c050', 'P1002', '53'), ('d58c11b6-0574-11e7-98aa-076d0efed018', '123', '36'), ('d6328394-0929-11e7-a01f-a8baaa26c050', 'P1002', '54'), ('d9c04c1c-0929-11e7-a01f-a8baaa26c050', 'P1002', '55'), ('ddd3d184-0929-11e7-a01f-a8baaa26c050', 'P1002', '56'), ('e2086a1c-0929-11e7-a01f-a8baaa26c050', 'P1002', '57'), ('e5f14644-0929-11e7-a01f-a8baaa26c050', 'P1002', '58'), ('ff5e8062-0bb6-11e7-8f64-57e474948c03', '20170318测试项目4', '91');
COMMIT;

-- ----------------------------
--  Table structure for `project_meeting`
-- ----------------------------
DROP TABLE IF EXISTS `project_meeting`;
CREATE TABLE `project_meeting` (
  `meetingId` varchar(7) NOT NULL COMMENT '会议编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `notifyDate` date DEFAULT NULL,
  `notifyMethod` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`meetingId`,`projectId`),
  KEY `FK_Reference_9` (`projectId`),
  CONSTRAINT `FK_MEETING` FOREIGN KEY (`meetingId`) REFERENCES `t_meeting_base_info` (`meetingId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Reference_8` FOREIGN KEY (`meetingId`) REFERENCES `t_meeting_base_info` (`meetingId`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目会议表';

-- ----------------------------
--  Records of `project_meeting`
-- ----------------------------
BEGIN;
INSERT INTO `project_meeting` VALUES ('M100001', 'P1000032', '2016-10-09', '微信'), ('M100002', 'P1001', '2016-10-31', 'email'), ('M100002', 'P1002', '2016-10-31', '电话'), ('M100003', 'ty20161207', '2016-12-07', '电话'), ('M100004', '2311123', '2017-02-08', '电话'), ('M100004', 'P1003', '2017-02-08', '电话'), ('M100005', '123', '2017-03-06', '电话'), ('M100006', '456', '2017-03-09', 'email'), ('M100007', '20170316测试项目', '2017-03-16', '电话'), ('M100008', '20170318测试项目', '2017-03-07', '电话'), ('M100009', '20170318测试项目3', '2017-03-18', '电话'), ('M100009', '20170318测试项目4', '2017-03-03', '电话');
COMMIT;

-- ----------------------------
--  Table structure for `project_verify`
-- ----------------------------
DROP TABLE IF EXISTS `project_verify`;
CREATE TABLE `project_verify` (
  `verifyId` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `completeDescription` longtext COMMENT '资料完善说明',
  `verifyFileNum` int(11) DEFAULT NULL,
  `verifyPerson` varchar(30) DEFAULT NULL,
  `verifyDate` date DEFAULT NULL,
  `verifyResult` longtext,
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`verifyId`),
  KEY `FK_Reference_19` (`projectId`),
  CONSTRAINT `FK_Reference_19` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='项目审核表';

-- ----------------------------
--  Records of `project_verify`
-- ----------------------------
BEGIN;
INSERT INTO `project_verify` VALUES ('1', 'P1000032', null, null, 'a', '2016-10-19', '通过', null), ('2', '456', 'fasdfsdfasdfads', '0', 'U100003', '2017-03-07', 'fsdafdsafadsf', null), ('4', '2311123', null, null, 'U100003', '2017-03-15', '', null), ('5', 'ty20161207', null, null, 'U100003', '2017-03-16', 'fadfa', null), ('6', 'P1001', null, null, 'U100003', '2017-03-16', 'dfadf', null), ('11', '20170316测试项目', 'ok', '2', 'U100003', '2017-03-17', 'OK', null), ('12', '20170318测试项目3', 'one', '0', null, null, null, null), ('13', '20170318测试项目3', 'two', '0', null, null, null, null), ('14', '20170318测试项目3', 'three', '0', null, null, null, null), ('15', '20170318测试项目3', 'scadSCFSDCASDCASDC', '0', 'U100003', '2017-03-23', '', null), ('16', '20170318测试项目', '1、完善科技项目验收申请书', '0', 'U100003', '2017-05-28', '通过', null);
COMMIT;

-- ----------------------------
--  Table structure for `role_module`
-- ----------------------------
DROP TABLE IF EXISTS `role_module`;
CREATE TABLE `role_module` (
  `roleId` varchar(40) NOT NULL COMMENT '编号',
  `moduleId` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`roleId`,`moduleId`),
  KEY `FK_MODULE` (`moduleId`),
  CONSTRAINT `FK_MODULE` FOREIGN KEY (`moduleId`) REFERENCES `t_base_defined_url` (`moduleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ROLE` FOREIGN KEY (`roleId`) REFERENCES `t_role_base_info` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限表';

-- ----------------------------
--  Records of `role_module`
-- ----------------------------
BEGIN;
INSERT INTO `role_module` VALUES ('c49b4a97-7a92-11e6-a8a9-206a8ab73904', '11'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '11'), ('c49b4a97-7a92-11e6-a8a9-206a8ab73904', '11001'), ('c49b4a97-7a92-11e6-a8a9-206a8ab73904', '11002'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '11003'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '11004'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '11005'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '12'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '12'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '12001'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '12002'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '12003'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '12004'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '12005'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '12006'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '12007'), ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '13'), ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '13002'), ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '13003'), ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '13004'), ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '13005'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '14'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '14001'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '14002'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '14003'), ('f3a432bb-7a92-11e6-a8a9-206a8ab739e7', '14003'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15001'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15002'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15003'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15004'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15005'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15006'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15007'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '15008');
COMMIT;

-- ----------------------------
--  Table structure for `specialist_arrangement`
-- ----------------------------
DROP TABLE IF EXISTS `specialist_arrangement`;
CREATE TABLE `specialist_arrangement` (
  `meetingId` varchar(7) NOT NULL COMMENT '会议编号',
  `specialistId` varchar(8) NOT NULL,
  `leader` varchar(8) DEFAULT NULL COMMENT '会议组长',
  `isAgree` bit(1) DEFAULT b'0',
  `source` varchar(50) DEFAULT NULL,
  `isPromise` bit(1) DEFAULT b'0',
  PRIMARY KEY (`meetingId`,`specialistId`),
  CONSTRAINT `FK_Reference_12` FOREIGN KEY (`meetingId`) REFERENCES `t_meeting_base_info` (`meetingId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `specialist_arrangement`
-- ----------------------------
BEGIN;
INSERT INTO `specialist_arrangement` VALUES ('M100001', 'SL100001', '组长', b'1', 'null', b'1'), ('M100002', 'SL100001', '组长', b'1', 'null', b'1'), ('M100002', 'SL100003', '副组长', b'1', 'null', b'1'), ('M100003', 'SL100001', null, b'1', 'null', b'1'), ('M100004', 'SL100002', '组长', b'1', 'null', b'1'), ('M100004', 'SL100004', null, b'1', 'null', b'1'), ('M100004', 'SL100005', '副组长', b'1', 'null', b'1'), ('M100005', 'SL100003', '组长', b'1', 'null', b'1'), ('M100006', 'SL100001', '组长', b'1', 'null', b'1'), ('M100007', 'SL100001', '组长', b'1', 'null', b'1'), ('M100008', 'SL100001', '组长', b'1', 'null', b'1'), ('M100008', 'SL100003', '副组长', b'1', 'null', b'1'), ('M100009', 'SL100001', '组长', b'1', 'null', b'1'), ('M100009', 'SL100002', '副组长', b'1', 'null', b'1');
COMMIT;

-- ----------------------------
--  Table structure for `specialist_evaluation`
-- ----------------------------
DROP TABLE IF EXISTS `specialist_evaluation`;
CREATE TABLE `specialist_evaluation` (
  `specialistId` varchar(8) NOT NULL,
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `reviewSuggestion` longtext NOT NULL COMMENT '评审意见',
  `opinion` text,
  `remark` varchar(50) DEFAULT '0' COMMENT '备注',
  PRIMARY KEY (`specialistId`,`projectId`),
  KEY `FK_Reference_37` (`projectId`),
  CONSTRAINT `FK_Reference_37` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专家评审表';

-- ----------------------------
--  Records of `specialist_evaluation`
-- ----------------------------
BEGIN;
INSERT INTO `specialist_evaluation` VALUES ('SL100001', '20170318测试项目', '该项目技术核心良好，通过。<div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/c1fdc38a-fa81-4c89-95ec-b26cfa5734bd.jpg\" style=\"float: right; width: 200px; height: 80px;\"></div>', '该项目技术核心良好，通过。', '1'), ('SL100001', '20170318测试项目3', 'OK<div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/9958eeb2-f992-4be2-89bc-db899cb31c35.png\" style=\"float: right; width: 200px; height: 80px;\"></div>', 'OK', '1'), ('SL100001', '20170318测试项目4', 'OK<br><div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/9958eeb2-f992-4be2-89bc-db899cb31c35.png\" style=\"float: right; width: 200px; height: 80px;\"></div>', 'OK<br>', '1'), ('SL100001', '456', '网，<div>额外人家乐扣，</div><div>福建省看到了</div><div>，飞机上等凉快</div><div>认为人类我</div><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/7ed1d04f-ea3a-4ee3-a3ae-58855c97e02e.PNG\" style=\"float: left; width: 400px; height: 180px;\"></div>', '网，<div>额外人家乐扣，</div><div>福建省看到了</div><div>，飞机上等凉快</div><div>认为人类我</div>', '1'), ('SL100001', 'P1001', '项目相当不错，值得推荐。<br>', '项目相当不错，值得推荐。<br>', '1'), ('SL100001', 'P1002', '<footer class=\"footer\" data-genuitec-lp-enabled=\"false\" data-genuitec-file-id=\"wc1-10\" data-genuitec-path=\"/Steap/WebRoot/admin/home/footer.jsp\">\n			<div class=\"center\">Copyright © 2015-2016 Tencent. All Rights Reserved.科技计划项目电子辅助验收及评估平台   技术支持联系方式：0351-6998011</div>\n			<div class=\"center\">版权所有 备案证号：晋ICP备050024564号</div>\n	<br><br></footer><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/a2e33531-a501-4aee-a544-3e196045daca.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '<footer class=\"footer\" data-genuitec-lp-enabled=\"false\" data-genuitec-file-id=\"wc1-10\" data-genuitec-path=\"/Steap/WebRoot/admin/home/footer.jsp\">\n			<div class=\"center\">Copyright © 2015-2016 Tencent. All Rights Reserved.科技计划项目电子辅助验收及评估平台   技术支持联系方式：0351-6998011</div>\n			<div class=\"center\">版权所有 备案证号：晋ICP备050024564号</div>\n	<br><br></footer>', '1'), ('SL100001', 'ty20161207', 'fkjwendfjg dgsdfg<div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/4c05d7f4-c408-41e2-9dcd-fea3bcec34aa.jpg\" style=\"float: left; width: 400px; height: 180px;\"></div>', 'fkjwendfjg dgsdfg', '1'), ('SL100002', '20170318测试项目3', 'ok <br><div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/672e2727-dd0d-497c-8d8e-770fab32bb85.png\" style=\"float: right; width: 200px; height: 80px;\"></div>', 'ok <br>', '1'), ('SL100002', '20170318测试项目4', 'mei you xiao<br><div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/672e2727-dd0d-497c-8d8e-770fab32bb85.png\" style=\"float: right; width: 200px; height: 80px;\"></div>', 'mei you xiao<br>', '1'), ('SL100002', '2311123', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/22c7b4e6-e358-4c14-a576-d600c89644ab.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>', '1'), ('SL100002', 'P1003', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><span style=\"color:rgb(255,0,0);\">某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。</span><br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/22c7b4e6-e358-4c14-a576-d600c89644ab.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><span style=\"color:rgb(255,0,0);\">某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。</span><br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br>', '1'), ('SL100003', '123', '沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八<br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/3fed21a8-bd61-4a8f-af2f-f9dab74074de.jpg\" style=\"float: left; width: 400px; height: 180px;\"></div>', '沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八沙发大八<br>', '1'), ('SL100003', '20170318测试项目', '该项目技术良好。<div class=\"div-container\" style=\"display: block; float: right; width: 200px; height: 80px;\"><br><br><br><img src=\"signature/a32bffb4-a0d4-4d0f-99b2-01be894dd6b5.jpeg\" style=\"float: right; width: 200px; height: 80px;\"></div>', '该项目技术良好。', '1'), ('SL100003', 'P1001', '快乐就好过分的空间和规范的留一条<div>健康很高发多少</div><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/0faf9923-c751-4054-8ebe-63fbb6e8fa1e.jpg\" style=\"float: left; width: 400px; height: 180px;\"></div>', '快乐就好过分的空间和规范的留一条<div>健康很高发多少</div>', '1'), ('SL100003', 'P1002', '；理解和广泛的撒凉快就好过分的<div>破坏广泛的撒聚义厅</div><div>头发</div><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/0faf9923-c751-4054-8ebe-63fbb6e8fa1e.jpg\" style=\"float: left; width: 400px; height: 180px;\"></div>', '；理解和广泛的撒凉快就好过分的<div>破坏广泛的撒聚义厅</div><div>头发</div>', '1'), ('SL100004', '2311123', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/aaf89fd2-9e45-4db5-b2bf-c949c595b2fe.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br>', '1'), ('SL100004', 'P1003', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/aaf89fd2-9e45-4db5-b2bf-c949c595b2fe.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br>某个专家把个人意见提交后，但当还需要修改时，root（管理员）用户可以将当前开会的某个专家对某项目的意见状态修改为【非提交】，这样专家可以修改评审意见。该功能是为了满足特例。<br><br>', '1'), ('SL100005', '2311123', '1、我一点都不喜欢测试，真希望有个专门的测试人员该多好啊。<br><br>2、但是目前这种状况，也只能这样子来处理吧？！<br><br>3、然而，我却不喜欢这样的评审过程。真令人难受！！！<br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/c6a8f263-eac3-4149-a932-5a676792c90f.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '1、我一点都不喜欢测试，真希望有个专门的测试人员该多好啊。<br><br>2、但是目前这种状况，也只能这样子来处理吧？！<br><br>3、然而，我却不喜欢这样的评审过程。真令人难受！！！<br>', '1'), ('SL100005', 'P1003', '.c_background {<br>&nbsp;&nbsp; &nbsp;position: absolute;<br>&nbsp;&nbsp; &nbsp;width: 100%;<br>&nbsp;&nbsp; &nbsp;height: 100%;<br>&nbsp;&nbsp; &nbsp;background-size: 100% 100%;<br>}<br>.c_background_top {<br>&nbsp;&nbsp; &nbsp;width: 100%;<br>&nbsp;&nbsp; &nbsp;height: 71.6%;<br>}<br><div class=\"div-container\" style=\"display: block; float: right; width: 400px; height: 180px;\"><img src=\"signature/c6a8f263-eac3-4149-a932-5a676792c90f.png\" style=\"float: left; width: 400px; height: 180px;\"></div>', '.c_background {<br>&nbsp;&nbsp; &nbsp;position: absolute;<br>&nbsp;&nbsp; &nbsp;width: 100%;<br>&nbsp;&nbsp; &nbsp;height: 100%;<br>&nbsp;&nbsp; &nbsp;background-size: 100% 100%;<br>}<br>.c_background_top {<br>&nbsp;&nbsp; &nbsp;width: 100%;<br>&nbsp;&nbsp; &nbsp;height: 71.6%;<br>}<br>', '1');
COMMIT;

-- ----------------------------
--  Table structure for `specialist_library`
-- ----------------------------
DROP TABLE IF EXISTS `specialist_library`;
CREATE TABLE `specialist_library` (
  `specialistId` varchar(8) NOT NULL COMMENT '专家编号',
  `name` varchar(10) NOT NULL COMMENT '姓名\r\n            ',
  `sex` varchar(6) NOT NULL COMMENT '性别',
  `account` varchar(30) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `signaturePass` varchar(16) DEFAULT NULL,
  `birthday` date NOT NULL COMMENT '出生年月',
  `engageDomain` varchar(16) NOT NULL COMMENT '从事专业\r\n            ',
  `duty` varchar(10) NOT NULL COMMENT '职务',
  `title` varchar(20) NOT NULL COMMENT '职称',
  `telephone` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `signature` varchar(50) DEFAULT NULL,
  `lastLoginTime` datetime DEFAULT NULL,
  `workUnit` varchar(30) DEFAULT NULL,
  `major` varchar(200) DEFAULT NULL,
  `degree` varchar(100) DEFAULT NULL,
  `ZJZY` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`specialistId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专家库';

-- ----------------------------
--  Records of `specialist_library`
-- ----------------------------
BEGIN;
INSERT INTO `specialist_library` VALUES ('SL100001', 'Lucene', '男', 'Lucene', '123456', '1', '1994-11-01', '信息领域', '专家', '高级专家', '18435155316', '359099631@qq.com', 'fbf52441-3d18-4a50-ab05-6fd9f7ce1a33.jpeg', null, '腾讯', null, null, null), ('SL100002', '张三', '女', '张三', '123456', '1', '1963-10-07', '交通运输领域', '教授', '教授', '13383546090', '13383546090@163.com', '672e2727-dd0d-497c-8d8e-770fab32bb85.png', null, '太原科技大学', null, null, null), ('SL100003', '李四', '女', '李四', '123456', '1', '1961-10-10', '交通运输领域', '教授', '教授', '13383546090', '13383546090@163.com', 'a32bffb4-a0d4-4d0f-99b2-01be894dd6b5.jpeg', null, '太原科技大学', null, null, null), ('SL100004', '王五', '男', '王五', '123456', '1', '1963-10-07', '交通运输领域', '教授', '教授', '13383646090', '1338354@163.com', 'aaf89fd2-9e45-4db5-b2bf-c949c595b2fe.png', null, '太原科技大学', null, null, null), ('SL100005', '李四', '女', '李小四', '123456', '1', '1991-10-21', '交通运输领域', '教授', '教授', '13383123490', '13383123490@163.com', '69b3594c-4d95-49cb-9c73-da0173ec3ca2.jpg', null, '太原科技大学', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `specialist_recommend`
-- ----------------------------
DROP TABLE IF EXISTS `specialist_recommend`;
CREATE TABLE `specialist_recommend` (
  `specialistId` varchar(8) NOT NULL COMMENT '专家编号',
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `name` varchar(30) NOT NULL COMMENT '姓名\r\n            ',
  `sex` varchar(6) NOT NULL COMMENT '性别',
  `password` varchar(16) DEFAULT NULL,
  `signaturePass` varchar(16) DEFAULT NULL,
  `birthday` date NOT NULL COMMENT '出生年月',
  `engageDomain` varchar(16) NOT NULL COMMENT '从事专业\r\n            ',
  `duty` varchar(10) NOT NULL COMMENT '职务',
  `title` varchar(20) NOT NULL COMMENT '职称',
  `telephone` varchar(13) NOT NULL COMMENT '联系电话',
  `email` varchar(100) DEFAULT NULL,
  `signature` varchar(50) DEFAULT NULL,
  `lastLoginTime` datetime DEFAULT NULL,
  `workUnit` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`specialistId`),
  KEY `FK_Reference_28` (`projectId`),
  CONSTRAINT `FK_Reference_28` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='专家推荐表';

-- ----------------------------
--  Records of `specialist_recommend`
-- ----------------------------
BEGIN;
INSERT INTO `specialist_recommend` VALUES ('SR100001', 'P1000032', 'Lucene', '男', null, null, '1994-11-01', '信息领域', '专家', '高级专家', '18435155316', '359099631@qq.com', null, null, '腾讯'), ('SR100002', 'P1001', '张三', '男', null, null, '1963-10-07', '交通运输领域', '教授', '教授', '13383546090', '13383546090@163.com', null, null, '太原科技大学'), ('SR100003', 'P1002', '李四', '女', null, null, '1991-10-21', '交通运输领域', '教授', '教授', '13383123490', '13383123490@163.com', null, null, '太原科技大学');
COMMIT;

-- ----------------------------
--  Table structure for `t_base_defined_url`
-- ----------------------------
DROP TABLE IF EXISTS `t_base_defined_url`;
CREATE TABLE `t_base_defined_url` (
  `id` varchar(40) NOT NULL COMMENT '机构编号',
  `moduleId` varchar(40) NOT NULL COMMENT '机构名称',
  `moduleName` varchar(100) NOT NULL COMMENT '是否启用',
  `moduleURL` varchar(200) DEFAULT NULL COMMENT '备注',
  `memo1` varchar(100) DEFAULT NULL,
  `momo2` varchar(100) DEFAULT NULL,
  `inUse` bit(1) DEFAULT b'1',
  `sortNo` int(11) DEFAULT NULL,
  `menuClass` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `moduleId` (`moduleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_base_defined_url`
-- ----------------------------
BEGIN;
INSERT INTO `t_base_defined_url` VALUES ('00bfacb5-723b-4268-8d1c-903068ad46d1', '15001', '部门管理', 'admin/unit/unit.jsp', '机构管理', null, b'1', '0', '2', '0'), ('03686ac8-71d6-11e6-a8a9-206a8ab73904', '15005', '角色管理', 'admin/sys/user/role.jsp', '角色管理', null, b'1', '0', '2', '0'), ('05284357-cc85-48dd-82eb-bffc33fcb44b', '11003', '领取验收资料', 'admin/receiveMaterial/getInvestigate.jsp', '领取验收资料', null, b'1', '0', '2', '0'), ('0d4abb66-0acf-11e7-8f0c-d69f9c2e13d4', '15008', '项目快速查询', 'admin/suprole/quickQuery.jsp', '项目快速查询', null, b'1', '0', '2', '0'), ('0ddfc928-78a5-11e6-a8a9-206a8ab73904', '12001', '创建会议', 'toMeetingManagePage.action', '创建会议', '', b'1', '0', '2', '0'), ('121e2105-a54d-499a-bfc5-f6a90335bb5b', '14001', '验收项目资料完善', 'admin/perfectMaterial/ProjectSearch.jsp', '验收项目资料完善', null, b'1', '0', '2', '0'), ('1a8e63cf-99c8-47b2-b1a3-f0b49ba2dd68', '14', '验收后管理', null, '验收后管理', null, b'1', '0', '1', '0'), ('24515a20-db8d-4ca1-ae8b-47dbcfcb65e3', '11002', '确定负责部门', 'admin/receiveMaterial/ConfirmDepartment.jsp', '确定负责部门', null, b'1', '0', '2', '0'), ('2b1e1669-92ca-4b40-869e-e5f1d5721dbc', '15', '系统管理', null, '系统管理', null, b'1', '0', '1', '0'), ('36ce3c00-0954-11e7-a01f-a8baaa26c050', '13003', '召开会议', 'admin/host/conveneMeeting.jsp', '召开会议', null, b'1', '0', '2', '0'), ('36ce66bc-0954-11e7-a01f-a8baaa26c050', '13004', '待主持会议', 'admin/host/hostedMeeting.jsp', '待主持会议', null, b'1', '0', '2', '0'), ('36f26c92-0954-11e7-a01f-a8baaa26c050', '13005', '已主持会议', 'admin/host/pastMeeting.jsp', '已主持会议', null, b'1', '0', '2', '0'), ('3f79c31d-78a5-11e6-a8a9-206a8ab73904', '12005', '通知企业', 'toMeetingManagePage2.action', '验收资料编制', null, b'1', '0', '2', '0'), ('441b3f60-ca17-490c-8e90-939b8a613670', '15002', '用户管理', 'admin/sys/user/user.jsp', '用户管理', null, b'1', '0', '2', '0'), ('5102bc52-a570-4011-91f4-23818b61d790', '13002', '会议主持首页', 'admin/host/host_meeting.jsp', '会议主持', null, b'1', '0', '2', '0'), ('5f9a4e38-b7bb-40a4-9eef-254869e1c0df', '11', '接受验收材料', null, '接受验收材料', null, b'1', '0', '1', '0'), ('62a038b2-78a5-11e6-a8a9-206a8ab73904', '12006', '企业准备资料上传', 'toMeetingManagePage2.action', '起草会议通知', null, b'1', '0', '2', '0'), ('749306a0-3272-46ca-b034-fb93943b3c94', '15004', '模块管理', 'admin/module/module.jsp', '模块管理', null, b'1', '0', '2', '0'), ('7d68d036-6c66-4ff4-86bd-afd0f484cdd0', '11001', '登记验收材料', 'admin/receiveMaterial/registerConfirm.jsp', '登记验收材料', null, b'1', '0', '2', '0'), ('82c85d89-78a5-11e6-a8a9-206a8ab73904', '12007', '资料编制', 'toMeetingManagePage2.action', '通知相关人员', null, b'1', '0', '2', '0'), ('8d81507d-a793-45ff-9f24-0b176437ca77', '14002', '专家意见修改审核', 'admin/meeting/ProjectSearch.jsp', '专家意见修改审核', null, b'1', '0', '2', '0'), ('9a55f82e-a59f-459e-8856-a54a514d8604', '15006', '专家管理', 'admin/specialist/specialist.jsp', '专家管理', null, b'1', '0', '2', '0'), ('a6150286-7b28-11e6-80ce-9890969cdf45', '12002', '确定验收专家', 'toMeetingManagePage.action', '确定验收专家 ', '', b'1', '0', '2', '0'), ('a87707c7-9136-4558-99dd-a286cec60883', '13', '验收会议管理', null, '验收会议管理', null, b'1', '0', '1', '0'), ('b8a3ef4b-7b28-11e6-80ce-9890969cdf45', '12003', '通知相关人员', 'toMeetingManagePage.action', '通知相关人员', '', b'1', '0', '2', '0'), ('c8889fff-c6ef-493d-a526-6a3491b77f86', '13001', '专家项目验收评审', null, '专家项目验收评审', null, b'1', '0', '2', '0'), ('ce486cea-7b28-11e6-80ce-9890969cdf45', '12004', '创建会议议程', 'toMeetingManagePage.action', '创建会议议程', '', b'1', '0', '2', '0'), ('d0de4967-615b-4f65-a0f6-57c7d7416466', '12', '验收会前准备', 'toMeetingManagePage.action', '验收会前准备', '', b'1', '0', '1', '0'), ('eb1b636e-e8fb-11e6-8d7f-d25221ae0921', '15007', '评审意见管理', 'admin/sys/opinion/evaluationOpinion.jsp', '评审意见管理', '', b'1', '0', '2', '0'), ('ef69b324-6d10-4f75-9a97-908cc6c60a1d', '11005', '企业现场考察', 'admin/receiveMaterial/EnterpriseInvestigate.jsp', '企业现场考察', null, b'1', '0', '2', '0'), ('f25a8b99-2ccc-4473-90a2-4201d4e9c33d', '14003', '验收证书发放', 'admin/issue/issue_certificate.jsp', '验收证书发放', null, b'1', '0', '2', '0'), ('f6349cf8-782a-4cb8-89d7-06659aa363d3', '15003', '字典管理', 'admin/sys/dictionary/dictionary.jsp', '字典管理', null, b'1', '0', '2', '0'), ('f7f90dac-3882-406b-8896-04ce8b256039', '11004', '审查验收资料', 'admin/receiveMaterial/getInvestigate.jsp', '审查验收资料', null, b'1', '0', '2', '0');
COMMIT;

-- ----------------------------
--  Table structure for `t_files_index`
-- ----------------------------
DROP TABLE IF EXISTS `t_files_index`;
CREATE TABLE `t_files_index` (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `fileType` varchar(6) DEFAULT NULL,
  `originalName` varchar(50) DEFAULT NULL,
  `currentName` varchar(50) DEFAULT NULL,
  `uploadTime` datetime DEFAULT NULL,
  `fileSize` varchar(10) DEFAULT NULL,
  `remark` bit(1) DEFAULT b'0',
  `opinionContent` longtext,
  PRIMARY KEY (`fileId`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_files_index`
-- ----------------------------
BEGIN;
INSERT INTO `t_files_index` VALUES ('1', '300002', 'Java7：核心技术与最佳实践.pdf', 'eacb0278-80d0-4a2d-a98f-a9c3cde1dea6.pdf', '2017-02-01 16:49:20', '51314KB', b'0', null), ('2', '300003', 'Effective.Java中文版.pdf', '4a49bdb8-689f-4428-832a-df92c8c0f8ab.pdf', '2017-02-01 16:49:35', '8664KB', b'0', null), ('3', '300004', 'Effective.Java中文版.pdf', 'a7298104-4302-4529-8894-b190aadfcb21.pdf', '2017-02-01 16:49:44', '8664KB', b'0', null), ('4', '300005', 'Effective.Java中文版.pdf', '476d1aa3-4897-43fb-b90d-f5613a097010.pdf', '2017-02-01 16:50:06', '8664KB', b'0', null), ('5', '300006', 'Effective.Java中文版.pdf', 'b49735b9-f665-4f81-b5f6-f9f3be0f7cf4.pdf', '2017-02-01 16:50:24', '8664KB', b'0', null), ('6', '300007', 'Effective.Java中文版.pdf', '6d2733c3-59b8-4214-a628-3a8efae2f2b0.pdf', '2017-02-01 16:50:39', '8664KB', b'1', null), ('7', '300008', 'Effective.Java中文版.pdf', '2a24e764-ae18-498f-b1ca-a85589b22ab5.pdf', '2017-02-01 16:50:56', '8664KB', b'0', null), ('8', '300002', 'Effective.Java中文版.pdf', 'f127b33a-fbe7-43e4-b3f2-324fa617863e.pdf', '2017-02-08 20:56:45', '8664KB', b'0', null), ('9', '300003', 'Effective.Java中文版.pdf', 'ba688a2f-2acc-42ad-a76d-71c4c190c9ad.pdf', '2017-02-08 20:56:49', '8664KB', b'0', null), ('10', '300004', 'Effective.Java中文版.pdf', 'bb2cf7c2-ed5b-4c3c-8d96-41adf4b3ed02.pdf', '2017-02-08 20:56:54', '8664KB', b'0', null), ('11', '300005', 'Effective.Java中文版.pdf', '6a2e9995-c07f-43bb-934c-85184e983352.pdf', '2017-02-08 20:56:59', '8664KB', b'0', null), ('12', '300006', 'Effective.Java中文版.pdf', '6b81d4f6-0c5f-4eb2-aa27-b7f0524b67d0.pdf', '2017-02-08 20:57:04', '8664KB', b'0', null), ('13', '300007', 'Effective.Java中文版.pdf', '93874419-f049-4bc4-9475-9ff3929262c3.pdf', '2017-02-08 20:57:08', '8664KB', b'0', null), ('14', '300008', 'Effective.Java中文版.pdf', '788392cd-0260-4268-9c0c-0eecec212b70.pdf', '2017-02-08 20:57:12', '8664KB', b'0', null), ('15', '300008', 'Effective.Java中文版.pdf', '720870dc-63d9-41f8-85dd-526fe56e8d9d.pdf', '2017-02-08 20:57:24', '8664KB', b'0', null), ('16', '300003', 'Effective.Java中文版.pdf', '2c542ba9-76c8-469d-9f58-0339903932a7.pdf', '2017-02-08 20:57:28', '8664KB', b'0', null), ('17', '300004', 'Effective.Java中文版.pdf', '8bcb1c46-9a51-4faf-ae62-a2fb653c1d84.pdf', '2017-02-08 20:57:32', '8664KB', b'0', null), ('18', '300005', 'Effective.Java中文版.pdf', '1242be0f-7c8b-4108-b641-2e81fcac17b9.pdf', '2017-02-08 20:57:36', '8664KB', b'0', null), ('19', '300006', 'Effective.Java中文版.pdf', '9a4a37c4-f359-40f7-86a8-4a249ba51b63.pdf', '2017-02-08 20:57:39', '8664KB', b'0', null), ('20', '300007', 'Effective.Java中文版.pdf', 'd17779c1-55eb-493e-8541-f24856bb6a19.pdf', '2017-02-08 20:57:43', '8664KB', b'0', null), ('21', '300008', 'Effective.Java中文版.pdf', '66a3bdf8-2ccb-4ae1-9016-9431d84b8c52.pdf', '2017-02-08 20:57:47', '8664KB', b'0', null), ('22', '300001', '东软方案.doc', '216affd1-2e2b-421a-a8b7-fe8a9679ccd8.doc', '2017-03-10 17:32:48', '198KB', b'0', null), ('23', '300001', '单词.docx', '05e84e98-5e5f-4b02-8070-8140825d203b.doc', '2017-03-10 17:33:04', '19KB', b'0', null), ('24', '300002', '阿里巴巴Java开发手册.pdf', 'c0122ae5-eaf2-4e73-912e-aac709f5555b.pdf', '2017-03-10 17:33:16', '762KB', b'0', null), ('25', '300003', '阿里巴巴Java开发手册.pdf', '375843d0-cd3b-4dd6-8659-2bd62e30f4f2.pdf', '2017-03-10 17:33:27', '762KB', b'0', null), ('26', '300004', '阿里巴巴Java开发手册.pdf', '01c74be0-e1d9-49cf-a141-fe1e5a68369c.pdf', '2017-03-10 17:33:36', '762KB', b'0', null), ('27', '300005', '阿里巴巴Java开发手册.pdf', '40dc7d3c-e9db-45a6-96f5-d089d0949f03.pdf', '2017-03-10 17:33:43', '762KB', b'0', null), ('28', '300006', '阿里巴巴Java开发手册.pdf', '039a5f5e-5353-4949-8adb-4075a2f9447e.pdf', '2017-03-10 17:33:52', '762KB', b'0', null), ('30', '300007', '阿里巴巴Java开发手册.pdf', '1fdb1656-5943-420a-b5cd-af1e1530b5b6.pdf', '2017-03-10 17:34:00', '762KB', b'0', null), ('31', '300008', '阿里巴巴Java开发手册.pdf', '5f6b543c-5212-4579-b90d-327d8896bafe.pdf', '2017-03-10 17:34:08', '762KB', b'0', null), ('32', '300003', '#ED539248.pdf', 'ed5f9c3f-73b7-483e-aca2-32ea04e935f9.pdf', '2017-03-10 17:34:14', '13114KB', b'0', null), ('36', '300004', '#book-rev-nov-1999.pdf', '9ebf4be9-7afc-4dbe-9f56-1b6d6375e3f2.pdf', '2017-03-10 17:35:01', '874KB', b'0', null), ('39', '300008', '#ED382935.pdf', 'd266ce0a-c81c-4844-b4cf-6626817db3c4.pdf', '2017-03-10 17:37:26', '339KB', b'0', null), ('40', '300005', '#book-rev-nov-1999.pdf', '4c92f44d-8927-4b2b-bb54-345aba627a37.pdf', '2017-03-10 17:37:44', '874KB', b'0', null), ('41', '300006', '#ED112300.pdf', 'd7afcfda-e733-466d-a006-f0624eb5d0e5.pdf', '2017-03-10 17:37:54', '445KB', b'0', null), ('42', '300005', '阿里巴巴Java开发手册.pdf', 'afe64366-5aff-4901-881b-657b98e737e5.pdf', '2017-03-10 17:49:00', '762KB', b'0', null), ('43', '300001', '20170307测试问题需要修改.doc', 'b511bb2e-5f49-44ae-a597-020699bcfd42.doc', '2017-03-15 10:45:58', '141KB', b'0', null), ('44', '300001', '健健简历.doc', '4583cdd8-c57b-4c8f-a075-5e1daa5c2bda.doc', '2017-03-15 10:46:47', '45KB', b'0', null), ('45', '300002', '丁英杰的简历.pdf', 'ff219803-1048-439a-8ef2-c6205b90e962.pdf', '2017-03-15 10:47:04', '115KB', b'0', null), ('46', '300003', '丁英杰的简历.pdf', '4bd3b9b4-f28d-43ce-82a4-fb21516b6acd.pdf', '2017-03-15 10:47:15', '115KB', b'0', null), ('47', '300004', '丁英杰的简历.pdf', '3f1dab84-143d-4d17-9dc6-d52dfa3f58b5.pdf', '2017-03-15 10:47:22', '115KB', b'0', null), ('48', '300005', '丁英杰的简历.pdf', '292e4954-4509-40ed-aa8e-5f727f604f67.pdf', '2017-03-15 10:47:29', '115KB', b'0', null), ('49', '300006', '丁英杰的简历.pdf', '930f9b43-ff29-45e0-8769-afb36bb30e96.pdf', '2017-03-15 10:47:39', '115KB', b'0', null), ('50', '300007', '丁英杰的简历.pdf', '034062f7-b8ba-4a9e-8063-9c10b7c59656.pdf', '2017-03-15 10:47:45', '115KB', b'0', null), ('51', '300008', '丁英杰的简历.pdf', '9d9e0256-7ff5-404e-887a-f1bba4c7cab0.pdf', '2017-03-15 10:47:52', '115KB', b'0', null), ('52', '300002', '丁英杰的简历.pdf', '6531b49c-abd4-44d8-8761-65f135dea8da.pdf', '2017-03-15 10:48:02', '115KB', b'0', null), ('53', '300003', '丁英杰的简历.pdf', 'bf4f0760-cf83-46c8-8fff-34ee6c02e287.pdf', '2017-03-15 10:48:08', '115KB', b'0', null), ('54', '300004', '丁英杰的简历.pdf', 'e95255ae-6831-44b1-b4c8-a115be3ad8aa.pdf', '2017-03-15 10:48:14', '115KB', b'0', null), ('55', '300005', '丁英杰的简历.pdf', '71938a3d-45ae-4950-b446-8c9951d06bd2.pdf', '2017-03-15 10:48:20', '115KB', b'0', null), ('56', '300006', '丁英杰的简历.pdf', '8ca41418-0d73-4ca8-ac14-6a4d766d9900.pdf', '2017-03-15 10:48:27', '115KB', b'0', null), ('57', '300007', '丁英杰的简历.pdf', '63757301-c169-47c3-afbc-996a312f5a07.pdf', '2017-03-15 10:48:34', '115KB', b'0', null), ('58', '300008', '丁英杰的简历.pdf', '96befdf7-5774-4446-be79-497f85d72da0.pdf', '2017-03-15 10:48:41', '115KB', b'0', null), ('69', '300002', 'Effective.Java中文版.pdf', 'f9dc589b-b6c4-48cd-8c06-774db69f4c2e.pdf', '2017-03-17 10:39:00', '8664KB', null, null), ('70', '300002', '视频-产品技术中心-JAVA开发工程师-张亚超-太原科技大学-15536542139.pdf', '3c3dcf47-6711-457a-abd3-d10e6f575591.pdf', '2017-03-17 10:53:03', '91KB', null, null), ('71', '300003', 'MVC的JavaScript Web富应用开发(完整版).pdf', 'f12d0214-cb65-4492-8889-67638823a184.pdf', '2017-03-17 10:53:19', '3840KB', null, null), ('72', '300002', 'MVC的JavaScript Web富应用开发(完整版).pdf', '31928001-4eda-48b4-b8d3-9d03b5f9e213.pdf', '2017-03-17 10:58:53', '3840KB', b'1', null), ('73', '300003', '阿里巴巴Java开发手册.pdf', '6faa0dfc-1ae5-43d3-af3a-275850b12051.pdf', '2017-03-17 10:59:14', '762KB', b'1', null), ('76', '300001', '教研室教学管理系统分析.docx', '45ab4dc0-33b7-4b7b-badf-d455c4721d58.doc', '2017-03-18 14:24:01', '127KB', null, null), ('77', '300001', '单词.docx', 'aef2ca60-c4df-43f6-85f7-92c92d6fa79c.doc', '2017-03-18 14:53:21', '19KB', null, null), ('78', '300001', '海贼王.docx', '5fb2ce41-f2d9-4c6d-a093-e1e2774040b6.doc', '2017-03-18 16:05:10', '23KB', null, null), ('82', '300004', '浅谈数据库主键的设计方法_钟兆超.pdf', '5cc331de-facd-4ed6-8c1e-d0a0b2b643f2.pdf', '2017-03-18 16:32:19', '110KB', null, null), ('85', '300003', '浅谈数据库主键设计的原则_常玉慧.pdf', 'fe0317bc-6edd-4009-97cc-092f08c3dd6d.pdf', '2017-03-18 16:42:00', '140KB', null, null), ('87', '300006', '浅谈数据库主键设计的原则_常玉慧.pdf', '4850472f-1a3d-4302-bcc3-a0cdbd1c5644.pdf', '2017-03-18 16:42:14', '140KB', null, null), ('88', '300005', '浅谈数据库主键设计的原则_常玉慧.pdf', '7b27624b-1429-43ab-aea7-6ffe60c9e58e.pdf', '2017-03-18 16:42:27', '140KB', null, null), ('91', '300008', '浅谈数据库主键的设计方法_钟兆超.pdf', '14e14da2-1dc5-4c64-b099-ac242ed568d9.pdf', '2017-03-18 16:43:45', '110KB', null, null), ('93', '300008', '浅谈数据库主键的设计方法_钟兆超.pdf', '4bfbad9b-83d7-42ae-833d-7d20d6d67fbe.pdf', '2017-03-18 16:44:50', '110KB', null, null), ('95', '300002', '浅谈数据库主键的设计方法_钟兆超.pdf', 'eb12b051-7a12-4ea4-9cfd-0dc49d6c1baf.pdf', '2017-03-18 16:46:47', '110KB', null, null), ('97', '300003', '数据库表主键设计方法的探讨_巫宗宾.pdf', 'fa51d398-9eba-4b02-b06d-6acbcc55f49e.pdf', '2017-03-18 16:48:03', '64KB', null, null), ('98', '300004', '数据库表主键设计方法的探讨_巫宗宾.pdf', 'f0859ac2-19d5-47f9-a748-132edc1ceed8.pdf', '2017-03-18 16:48:10', '64KB', null, null), ('99', '300005', '数据库表主键设计方法的探讨_巫宗宾.pdf', '39b242d5-436a-4c74-9c0f-22a920fa2557.pdf', '2017-03-18 16:48:15', '64KB', null, null), ('100', '300006', '数据库表主键设计方法的探讨_巫宗宾.pdf', 'e9544426-89cc-4de2-a18b-fb1fd54cc0eb.pdf', '2017-03-18 16:48:20', '64KB', null, null), ('101', '300007', '数据库表主键设计方法的探讨_巫宗宾.pdf', '4563fb38-685c-438d-8833-8667ea8fccb4.pdf', '2017-03-18 16:48:31', '64KB', null, null), ('102', '300008', '数据库表主键设计方法的探讨_巫宗宾.pdf', 'b2976f50-8df1-4226-89c6-ce1a0113042e.pdf', '2017-03-18 16:48:37', '64KB', null, null), ('126', '300001', 'fa6d4e71-4539-4958-8301-3c0c5d0d0280', 'ac177e81-63cb-404c-baea-c3ddec09d9db', '2017-03-22 18:06:34', '0KB', b'0', '<div><b>Struts 2 (FreeMarker、Velocity) &nbsp; SpringMVC 4</b></div><div><b>Spring 4</b></div><div><b>Mybatis 3 + Hibernate 4</b></div><div><b>MySQL、Oracle</b></div><div><b>nginx + tomcat/WebLogic &nbsp; &nbsp;jetty</b></div><div><b>memcached/redis、mongodb</b></div><div><b>WebService、Restful</b></div><div><b>数据库连接池 （阿里巴巴） Druid</b></div><div><b>项目安全框架 Shiro（可验证不同的url请求）</b></div><div><b>Lucene全站检索功能</b></div><div><b>设计模式、Internet基本协议（如TCP/IP、HTTP等）内容及相关应用</b></div><div><b>Bootstrap 3 UI框架进行前端界面设计</b></div><div><b>EasyUI框架进行后台界面设计</b></div><div><b>百度编辑器Ueditor</b></div><div><b>优化算法、算法与数据结构</b></div><div><b>分布式集群</b></div><div><br></div>'), ('129', '300005', 'Java7：核心技术与最佳实践.pdf', '24c8f442-432d-43a7-94a6-a9650f0cf0df.pdf', '2017-03-23 10:37:30', '51314KB', b'1', null), ('131', '300006', 'Java 虚拟机面试题全面解析.pdf', '312e0cc2-60da-44ef-84c2-94406ef953f0.pdf', '2017-03-23 10:39:36', '1024KB', b'1', null), ('132', '300002', '阿里巴巴Java开发手册.pdf', '888717de-759d-43c9-90de-b9319d69aed7.pdf', '2017-03-31 13:39:17', '762KB', b'0', null), ('133', '300002', '神奇的矩阵第二季.pdf', '8a9328f3-fdbb-45fc-a15d-a6047ee53e8e.pdf', '2017-05-28 13:50:08', '2633KB', b'0', null), ('134', '300003', '线代思维.pdf', '5e80f10f-217f-48d1-b760-4c11a85dd733.pdf', '2017-05-28 13:50:30', '7717KB', b'0', null), ('135', '300004', '线代思维.pdf', '4c859c55-0cf9-4dc3-86b6-94f071fb9400.pdf', '2017-05-28 13:50:35', '7717KB', b'0', null), ('136', '300005', '线代思维.pdf', '0bbdb790-9d1e-48a6-948d-4ccff2aaa492.pdf', '2017-05-28 13:50:39', '7717KB', b'0', null), ('137', '300006', '线代思维.pdf', 'bc68c551-3682-48de-b8e7-245fd5fe3de5.pdf', '2017-05-28 13:50:44', '7717KB', b'0', null), ('138', '300007', '线代思维.pdf', '31547c52-7c4d-4252-80b7-c0d486cb802c.pdf', '2017-05-28 13:50:49', '7717KB', b'0', null), ('139', '300008', '线代思维.pdf', '9e0e93d3-3874-4023-a9fd-2996ce743470.pdf', '2017-05-28 13:50:56', '7717KB', b'0', null), ('140', '300002', '阿里巴巴Java开发手册.pdf', 'e364fe32-2eef-4dd0-8538-7f7701214e62.pdf', '2017-05-28 14:01:34', '762KB', b'1', null), ('141', '300002', 'One commit存在的问题与总结.pdf', '0c962719-03e6-4b43-a9fc-ff16a26f4aad.pdf', '2017-06-14 14:31:40', '190KB', b'0', null), ('142', '300002', 'Two commit存在的问题与总结.pdf', '1b1f864b-87ef-4549-8be1-3ed476a1aec2.pdf', '2017-06-14 14:38:05', '130KB', b'0', null), ('143', '300003', 'Two commit存在的问题与总结.pdf', '5b6ce2de-4fbe-481e-9927-c4a5217dd23e.pdf', '2017-06-14 14:38:09', '130KB', b'0', null), ('144', '300004', 'Two commit存在的问题与总结.pdf', '657bdd56-c5b4-4612-8dc1-f66a8660b8f7.pdf', '2017-06-14 14:38:12', '130KB', b'0', null), ('145', '300005', 'Two commit存在的问题与总结.pdf', '26920f38-5b60-4957-99bb-70eef825d9cb.pdf', '2017-06-14 14:38:16', '130KB', b'0', null), ('146', '300006', 'Two commit存在的问题与总结.pdf', '15b1ecca-b16c-4771-941d-acf30e8ecb80.pdf', '2017-06-14 14:38:19', '130KB', b'0', null), ('147', '300007', 'Two commit存在的问题与总结.pdf', '915aeab8-51cd-4de1-9339-7a6a0ffc7e41.pdf', '2017-06-14 14:38:23', '130KB', b'0', null), ('148', '300008', 'Two commit存在的问题与总结.pdf', 'b32b96b3-43d4-4b18-b5ce-47b3778fddba.pdf', '2017-06-14 14:38:26', '130KB', b'0', null);
COMMIT;

-- ----------------------------
--  Table structure for `t_meeting_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_meeting_base_info`;
CREATE TABLE `t_meeting_base_info` (
  `meetingId` varchar(7) NOT NULL COMMENT '会议编号',
  `hostId` varchar(40) DEFAULT NULL COMMENT '编号',
  `meetingName` varchar(60) DEFAULT NULL,
  `meetingTime` datetime NOT NULL COMMENT '会议时间',
  `meetingPlace` varchar(60) DEFAULT NULL,
  `meetingNotice` varchar(50) DEFAULT NULL,
  `meetingStatus` varchar(6) DEFAULT '200001' COMMENT '会议状态',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`meetingId`),
  KEY `FK_Reference_33` (`hostId`),
  CONSTRAINT `FK_Reference_33` FOREIGN KEY (`hostId`) REFERENCES `t_user_base_info` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会议基本信息表';

-- ----------------------------
--  Records of `t_meeting_base_info`
-- ----------------------------
BEGIN;
INSERT INTO `t_meeting_base_info` VALUES ('M100001', 'U100005', '2016 Iphone销量', '2016-10-09 10:48:35', '科大', null, '200003', '2016 Iphone销量'), ('M100002', 'U100005', 'jintian de huoiyi ', '2016-10-31 14:50:31', 'TAIYUAN', null, '200003', 'iuyhfds'), ('M100003', 'U100005', '2017 Iphone 6S销量', '2016-12-07 23:13:09', '科大', null, '200003', 'ok'), ('M100004', 'U100005', '郑州', '2017-02-08 20:53:41', '郑州', null, '200003', '郑州'), ('M100005', 'U100007', '太原太原太原', '2017-03-10 17:30:40', '太原太原太原', null, '200003', '玩儿同意'), ('M100006', 'U100005', '太原2太原2', '2017-03-10 17:31:17', '太原2太原2', null, '200003', '太原2太原2'), ('M100007', 'U100005', '20170316测试项目', '2017-03-16 18:44:34', '20170316测试项目', null, '200002', '20170316测试项目'), ('M100008', 'U100005', '20170318测试会议', '2017-03-18 14:45:17', '科大', null, '200003', '20170318测试项目'), ('M100009', 'U100005', '20170318测试会议', '2017-03-18 16:01:14', '科技大学', null, '200003', '20170318测试会议');
COMMIT;

-- ----------------------------
--  Table structure for `t_pictures_index`
-- ----------------------------
DROP TABLE IF EXISTS `t_pictures_index`;
CREATE TABLE `t_pictures_index` (
  `pictureId` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `pictureType` varchar(6) DEFAULT NULL COMMENT '图片类型',
  `originalName` varchar(50) DEFAULT NULL COMMENT '图片原始名称',
  `currentName` varchar(50) DEFAULT NULL,
  `uploadTime` datetime DEFAULT NULL,
  `pictureSize` varchar(10) DEFAULT NULL,
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`pictureId`),
  KEY `FK_Reference_26` (`projectId`),
  CONSTRAINT `FK_Reference_26` FOREIGN KEY (`projectId`) REFERENCES `t_project_base_info` (`projectId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8 COMMENT='图片';

-- ----------------------------
--  Records of `t_pictures_index`
-- ----------------------------
BEGIN;
INSERT INTO `t_pictures_index` VALUES ('1', 'P1000032', '400001', 'psb-2.jpeg', 'd4f4935d-9fd2-4bf7-8897-ac9362fe5e45.jpeg', '2016-10-09 10:43:05', '34643', null), ('2', 'P1000032', '400001', 'psb-3.jpeg', 'fb3c72ee-1704-457e-8ea2-2447336bb092.jpeg', '2016-10-09 10:43:05', '11446', null), ('3', 'P1000032', '400001', 'psb.jpeg', '26bc84b2-d8b8-42cc-b194-1a8ce735d5d1.jpeg', '2016-10-09 10:43:05', '24196', null), ('4', 'P1000032', '400002', 'psb.jpeg', '0f24eb6f-e4b9-42be-abc1-3ec4a06af48d.jpeg', '2016-10-09 10:43:28', '24196', null), ('5', 'P1000032', '400003', 'psb.jpeg', '9b953673-d019-4881-acce-bc6dc5635a85.jpeg', '2016-10-09 11:38:40', '24KB', null), ('6', 'P1001', '400001', '6.jpg', '54cd576c-15ba-47f2-8cad-d90edb11f35c.jpg', '2016-10-31 14:31:12', '124390', null), ('7', 'P1001', '400001', '7.jpg', '5debaacf-0356-4eef-9668-e910b21c9dd9.jpg', '2016-10-31 14:31:12', '45908', null), ('8', 'P1001', '400001', '10.jpg', '6a3edfeb-115e-4062-8028-7d82620fa4ba.jpg', '2016-10-31 14:31:12', '102315', null), ('9', 'P1001', '400002', '1.jpg', '1b2b4ccf-d543-4b30-b36f-1e6003cad559.jpg', '2016-10-31 14:31:45', '101015', null), ('10', 'P1001', '400002', '4.jpg', 'cd95b4af-c2ed-4866-83d0-57dda8b36786.jpg', '2016-10-31 14:31:45', '177195', null), ('11', 'P1001', '400002', '8.jpg', 'afdb94e7-aadf-4aaf-b28f-b0c07822a125.jpg', '2016-10-31 14:31:45', '163731', null), ('12', 'P1002', '400001', '3.jpg', 'db35b58f-1694-4667-a094-72041875a4b4.jpg', '2016-10-31 14:37:32', '159789', null), ('13', 'P1002', '400001', '6.jpg', '3bfc897f-6984-445a-9b99-a530256b32dc.jpg', '2016-10-31 14:37:32', '124390', null), ('14', 'P1002', '400001', '7.jpg', '4302c5d8-1290-42a8-a3b1-5d3c8fc39186.jpg', '2016-10-31 14:37:32', '45908', null), ('15', 'P1002', '400002', '5.jpg', '1e3c4eec-5311-4abc-ab5f-85b04e82bad6.jpg', '2016-10-31 14:37:56', '31265', null), ('16', 'P1002', '400002', '9.jpg', '875148c2-a821-444b-8f42-f0b031ea93ed.jpg', '2016-10-31 14:37:57', '527496', null), ('17', 'ty20161207', '400001', '04990FE81BD207C56B6B2DE3A4BDD113.jpg', 'b3c273fd-adb3-48e8-ba48-7e0668e3da91.jpg', '2016-12-07 23:12:38', '267960', null), ('18', 'ty20161207', '400001', '屏幕快照 2016-04-21 下午11.56.14.png', 'f7208811-c890-4341-bf11-981c1d2bc500.png', '2016-12-07 23:12:38', '94625', null), ('19', 'ty20161207', '400001', '屏幕快照 2016-05-14 下午9.48.45.png', 'ccd5c1e4-fe94-4247-b2eb-4787d5d201d3.png', '2016-12-07 23:12:38', '405725', null), ('20', 'ty20161207', '400001', '屏幕快照 2016-05-14 下午9.48.50.png', '9eb8de50-9a33-47d0-9887-30834e4ea3f9.png', '2016-12-07 23:12:38', '355908', null), ('21', 'ty20161207', '400001', '屏幕快照 2016-08-19 上午12.33.13.png', '296fdbbc-eacf-405c-ae34-83c62c3976db.png', '2016-12-07 23:12:38', '11917', null), ('22', 'ty20161207', '400001', '屏幕快照 2016-08-24 上午12.38.36.png', 'e9f40dca-9235-41e6-af0b-82c6f25b3ff1.png', '2016-12-07 23:12:38', '693270', null), ('23', 'ty20161207', '400001', '屏幕快照 2016-09-05 上午8.37.05.png', '6fc74eda-7244-4a7d-9472-338cc47c9718.png', '2016-12-07 23:12:38', '175512', null), ('24', 'ty20161207', '400001', '屏幕快照 2016-09-05 上午10.33.15.png', '227c939c-1e2e-432d-8b33-67996c4fae3f.png', '2016-12-07 23:12:38', '26264', null), ('25', 'ty20161207', '400001', '屏幕快照 2016-09-12 上午12.07.50.png', 'e378e6a1-0d02-409a-98b0-0bd2553a18a0.png', '2016-12-07 23:12:38', '371947', null), ('26', 'ty20161207', '400001', '屏幕快照 2016-09-12 上午12.17.43.png', '65fd1752-80b4-4530-8b17-eb6030a5f1c5.png', '2016-12-07 23:12:38', '91579', null), ('27', 'ty20161207', '400001', '屏幕快照 2016-09-18 下午9.33.20.png', 'e1a00d0e-f0bc-4e36-838e-4f1a49f4fe2b.png', '2016-12-07 23:12:38', '1267574', null), ('28', 'ty20161207', '400001', '屏幕快照 2016-11-02 上午11.27.02.png', '92d6ab01-bce0-47ac-a4f5-5754d5d3535c.png', '2016-12-07 23:12:38', '166206', null), ('29', 'ty20161207', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', '3efb4ae0-f603-4f4c-8099-5c3a81655c1e.png', '2016-12-07 23:12:38', '199686', null), ('30', 'ty20161207', '400001', '屏幕快照 2016-11-07 下午8.11.43.png', 'e8299414-2ab3-45e8-940b-13bcd98d1a03.png', '2016-12-07 23:12:38', '256560', null), ('31', 'ty20161207', '400001', '屏幕快照 2016-11-07 下午8.12.19.png', '0dee62e2-5532-4d34-aee3-b4ddf21d5a79.png', '2016-12-07 23:12:39', '112786', null), ('32', 'ty20161207', '400001', '屏幕快照 2016-11-07 下午8.14.41.png', 'bb9e34be-f5b4-43a4-b487-e346d4637b82.png', '2016-12-07 23:12:39', '552523', null), ('33', 'ty20161207', '400001', '屏幕快照 2016-11-07 下午8.20.40.png', '9665a394-0474-4b94-b67a-92427a13d39d.png', '2016-12-07 23:12:39', '263837', null), ('34', 'ty20161207', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '2ea0a0d7-d7ef-4ae7-b24a-4b29eb5d47f8.png', '2016-12-07 23:12:39', '408947', null), ('35', 'ty20161207', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', '0e5eb464-6a53-4fdd-8e99-ad0919fd0871.png', '2016-12-07 23:12:39', '331789', null), ('36', 'ty20161207', '400001', '屏幕快照 2016-11-09 下午10.00.05.png', '39ff8177-9489-49d8-9683-1b991cedf380.png', '2016-12-07 23:12:39', '721384', null), ('37', 'ty20161207', '400001', '屏幕快照 2016-11-10 下午4.58.16.png', 'c62d50fc-e18d-4358-8474-dc95c01157c4.png', '2016-12-07 23:12:39', '288201', null), ('38', 'ty20161207', '400001', '屏幕快照 2016-11-12 下午2.26.55.png', '06eb660c-21e7-4742-a1d6-4a9632bf2151.png', '2016-12-07 23:12:39', '318378', null), ('39', 'ty20161207', '400001', '屏幕快照 2016-11-14 下午4.49.16.png', 'c799593d-1c02-4d1b-aef5-1381d895af56.png', '2016-12-07 23:12:39', '245872', null), ('40', 'ty20161207', '400003', '04990FE81BD207C56B6B2DE3A4BDD113.jpg', '8bd3b919-df6e-4669-be34-5a19d1d835be.jpg', '2016-12-07 23:19:03', '267KB', null), ('41', 'P101', '400001', '001.JPG', '25a5483e-97e2-4215-994f-f7d7f606a24a.JPG', '2016-12-08 14:33:47', '733774', null), ('42', 'P101', '400001', '071.JPG', 'fc52e896-98ad-4e9a-b13d-41acd9bc9b33.JPG', '2016-12-08 14:33:50', '4414594', null), ('43', 'P101', '400001', '156.JPG', '03995aa7-8414-4f03-9ec5-521d3906fdb6.JPG', '2016-12-08 14:33:56', '7429755', null), ('44', 'P101', '400001', '168.JPG', 'c812c359-b948-4d41-85d8-e436e8db47bb.JPG', '2016-12-08 14:34:02', '7466424', null), ('45', 'P101', '400001', '169.JPG', '07199a55-24c6-4870-84a0-a3d6ffd662b1.JPG', '2016-12-08 14:34:07', '8091049', null), ('53', 'dasdadasda', '400002', '001.JPG', 'eb4b7c79-c00d-41dd-b31b-383d0c36883c.JPG', '2016-12-08 14:38:58', '733774', null), ('54', 'dasdadasda', '400002', '156.JPG', 'f13dc455-4761-46d1-a4fe-6a124889fdbe.JPG', '2016-12-08 14:39:20', '7429755', null), ('55', 'dasdadasda', '400002', '176354_2013070319585100_20130703_195851.jpg', 'ee8fc1d3-cc0b-4ba1-9611-8a01b4b6eb55.jpg', '2016-12-08 14:39:40', '2042035', null), ('56', 'dasdadasda', '400001', '169.JPG', '0ca7affd-af4a-4077-b1a5-5eb314dfb961.JPG', '2016-12-08 14:39:49', '8091049', null), ('57', 'dasdadasda', '400001', '170.JPG', '6ca26679-d115-461d-8104-daee741071dc.JPG', '2016-12-08 14:39:51', '8088429', null), ('58', 'dasdadasda', '400001', '171.JPG', 'c58684b1-dc55-496b-b851-a5dae7171cdb.JPG', '2016-12-08 14:39:54', '7296462', null), ('59', 'dasdadasda', '400001', '173.JPG', '2e948efd-c128-4d1d-9a2b-9597e9b36a9a.JPG', '2016-12-08 14:39:55', '6145629', null), ('60', '2311123', '400001', '屏幕快照 2016-08-24 上午12.38.36.png', 'f744f207-372f-4306-88fe-b1d40ece27d4.png', '2017-02-08 20:52:18', '693270', null), ('61', '2311123', '400001', '屏幕快照 2016-09-05 上午10.33.15.png', '12417ada-3d85-4501-931d-bcedcbd72272.png', '2017-02-08 20:52:19', '26264', null), ('62', '2311123', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', 'ba8de5f1-5532-4525-89e0-3646a167c7e5.png', '2017-02-08 20:52:19', '199686', null), ('63', '2311123', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '4f1ce5a6-0b0e-427e-a1e5-ddd4b384124e.png', '2017-02-08 20:52:19', '408947', null), ('64', '2311123', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', '2537f95c-1653-491e-9f71-b3731294fbce.png', '2017-02-08 20:52:19', '331789', null), ('65', '2311123', '400002', '屏幕快照 2016-11-09 下午9.35.47.png', '03ad1d27-7305-4ae1-864c-275af4f42b68.png', '2017-02-08 20:52:34', '331789', null), ('66', '2311123', '400002', '屏幕快照 2016-11-10 下午4.58.16.png', '375a60ad-d780-4dd5-843c-50b0d012eeea.png', '2017-02-08 20:52:34', '288201', null), ('67', '2311123', '400002', '屏幕快照 2016-11-14 下午4.49.16.png', '30f1b4c8-cdd1-4c37-bb82-3d9046d21435.png', '2017-02-08 20:52:34', '245872', null), ('68', '2311123', '400002', '屏幕快照 2016-11-21 下午10.21.10.png', '340d9c29-2b1e-4376-a8b4-9f69cf7c7f06.png', '2017-02-08 20:52:34', '278042', null), ('69', '2311123', '400002', '屏幕快照 2016-12-10 下午12.11.44.png', '1826c9f5-d31b-497e-8e08-016c881bcf92.png', '2017-02-08 20:52:34', '231052', null), ('70', '2311123', '400002', '屏幕快照 2016-12-14 下午9.56.42.png', 'aba4e6cd-e580-42b6-940b-d8d1cbbbc52a.png', '2017-02-08 20:52:34', '210998', null), ('71', 'P1003', '400001', '屏幕快照 2016-05-14 下午9.48.45.png', 'cb60287a-bcea-45af-94f5-47c64f963c1e.png', '2017-02-08 20:52:55', '405725', null), ('72', 'P1003', '400001', '屏幕快照 2016-05-14 下午9.48.50.png', '61668a2c-be23-4346-b04c-daf054f04f66.png', '2017-02-08 20:52:55', '355908', null), ('73', 'P1003', '400001', '屏幕快照 2016-08-19 上午12.33.13.png', 'e88027f5-1196-4008-ad14-d2589c563c06.png', '2017-02-08 20:52:55', '11917', null), ('74', 'P1003', '400001', '屏幕快照 2016-08-24 上午12.38.36.png', 'e2c0b346-55e6-48e1-ba42-9523a8c0d37b.png', '2017-02-08 20:52:55', '693270', null), ('75', 'P1003', '400001', '屏幕快照 2016-09-05 上午10.33.15.png', '72a8930e-c278-4be3-a670-5f0a3de4b3cf.png', '2017-02-08 20:52:55', '26264', null), ('76', 'P1003', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', '0d8db5f4-fd5b-4b84-9421-6493ba1fbeb7.png', '2017-02-08 20:52:55', '199686', null), ('77', 'P1003', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '162b6582-a948-45c5-a31c-8f96fc8e38e2.png', '2017-02-08 20:52:55', '408947', null), ('78', 'P1003', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', 'ab02802a-8704-43fd-9515-8daea5242247.png', '2017-02-08 20:52:55', '331789', null), ('79', '2311123', '400003', '屏幕快照 2016-11-21 下午10.21.10.png', 'bcb09b12-215c-42f2-9f8b-0d0300ff99a7.png', '2017-02-08 20:55:59', '278KB', null), ('80', 'P1003', '400003', '屏幕快照 2016-11-14 下午4.49.16.png', 'c6a7e4f0-6f49-428f-a3cd-4a1e475f9718.png', '2017-02-08 20:56:06', '245KB', null), ('81', '123', '400001', '1484026611290.jpg', '78951a75-6545-4c07-b46f-570d7e48bfc1.jpg', '2017-03-10 17:28:39', '173221', null), ('82', '123', '400001', '1484026638761.jpg', '6171d7d2-569d-4396-8ab9-0627a879e6d0.jpg', '2017-03-10 17:28:39', '148829', null), ('83', '123', '400001', '1484026723194.jpg', 'a522856b-6b7d-49d2-9ff0-a8e3a4410e9a.jpg', '2017-03-10 17:28:40', '151175', null), ('84', '456', '400001', 'QQ图片20170304220728.jpg', '3681cb9e-7694-4948-970a-252e3f189ae7.jpg', '2017-03-10 17:29:33', '59805', null), ('85', '456', '400002', 'QQ图片20170304220728.jpg', '11936f8c-f72c-400f-a6a7-a347b2a0258e.jpg', '2017-03-10 17:29:53', '59805', null), ('86', '123', '400003', 'QQ图片20170304220728.jpg', 'b68d32b2-e9d0-43d8-b305-9cede87b0ae0.jpg', '2017-03-10 17:33:30', '59KB', null), ('87', 'P1001', '400003', 'QQ图片20170121211422.jpg', '88aab3da-f502-4ec9-9cc9-53a4cc18128e.jpg', '2017-03-15 10:46:16', '155KB', null), ('88', 'P1002', '400003', 'QQ图片20170121211422.jpg', 'c87900e0-e16e-4594-b88b-0d63787a4a6b.jpg', '2017-03-15 10:46:38', '155KB', null), ('89', '20170316测试项目', '400001', '屏幕快照 2016-04-21 下午11.56.14.png', '9c0b79ab-13a1-4efc-ba2e-2dc2e095663e.png', '2017-03-16 18:42:40', '94625', null), ('90', '20170316测试项目', '400001', '屏幕快照 2016-05-14 下午9.48.45.png', '037d19c4-446f-4ee6-9b86-f990bdc88cf9.png', '2017-03-16 18:42:40', '405725', null), ('91', '20170316测试项目', '400001', '屏幕快照 2016-05-14 下午9.48.50.png', '092532fe-697a-4611-949a-8b10d3ae0e01.png', '2017-03-16 18:42:40', '355908', null), ('92', '20170316测试项目', '400001', '屏幕快照 2016-08-24 上午12.38.36.png', 'c9206c10-c812-45f9-8c6a-f44f5bd96a42.png', '2017-03-16 18:42:40', '693270', null), ('93', '20170316测试项目', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', '1d7d551e-bd80-4032-a6c7-22d804c9a01a.png', '2017-03-16 18:42:40', '199686', null), ('94', '20170316测试项目', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '393ec188-6abb-4c6f-afcb-31f9efc5db86.png', '2017-03-16 18:42:40', '408947', null), ('95', '20170316测试项目', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', '00b9ddb3-395a-4f4e-a1d9-1be6fd7d8d70.png', '2017-03-16 18:42:40', '331789', null), ('96', '20170316测试项目', '400002', '屏幕快照 2016-05-14 下午9.48.45.png', '06d47a65-98b6-420a-9351-7ee7e0db4309.png', '2017-03-16 18:44:11', '405725', null), ('97', '20170316测试项目', '400002', '屏幕快照 2016-05-14 下午9.48.50.png', '1bc12ad6-5456-48ad-880d-091e82033f72.png', '2017-03-16 18:44:11', '355908', null), ('98', '20170316测试项目', '400002', '屏幕快照 2016-08-24 上午12.38.36.png', '66192d2e-d2d8-4e63-87f5-8deb438a976f.png', '2017-03-16 18:44:11', '693270', null), ('99', '20170316测试项目', '400002', '屏幕快照 2016-11-07 下午5.19.44.png', '3b94b7a3-587c-49ac-86be-ef41c4ae1e41.png', '2017-03-16 18:44:11', '199686', null), ('100', '20170316测试项目', '400002', '屏幕快照 2016-11-09 下午5.39.20.png', 'aaadc995-321e-42e1-a8e0-41c717e7c513.png', '2017-03-16 18:44:11', '408947', null), ('101', '20170316测试项目', '400002', '屏幕快照 2016-11-09 下午9.35.47.png', '61084dff-0b3d-4009-a61f-6d743f8ede77.png', '2017-03-16 18:44:11', '331789', null), ('102', '20170316测试项目', '400002', '屏幕快照 2016-11-10 下午4.58.16.png', '1a10c27a-946d-41c7-bf09-219c582aa3f2.png', '2017-03-16 18:44:11', '288201', null), ('103', '20170316测试项目', '400002', '屏幕快照 2016-11-14 下午4.49.16.png', 'f7db6ddb-2dbe-4df3-a71a-d424b616af6d.png', '2017-03-16 18:44:11', '245872', null), ('104', '20170316测试项目', '400002', '屏幕快照 2016-11-21 下午10.21.10.png', 'fb5bceae-e4ab-4502-8a22-4fd77f26e4a9.png', '2017-03-16 18:44:11', '278042', null), ('105', '20170316测试项目', '400002', '屏幕快照 2016-12-10 下午12.11.44.png', '1af4e1d7-8d8a-4adb-a1af-ee368067b0e8.png', '2017-03-16 18:44:11', '231052', null), ('106', '20170316测试项目', '400003', '屏幕快照 2016-11-14 下午4.49.16.png', 'f24a7e97-4969-40e2-a21d-ebf15e345c23.png', '2017-03-16 18:46:48', '245KB', null), ('107', '20170318测试项目', '400001', '屏幕快照 2016-11-10 下午4.58.16.png', '38579745-807c-40a9-b0ac-e998f06102a7.png', '2017-03-18 14:42:08', '288201', null), ('108', '20170318测试项目', '400001', '屏幕快照 2016-11-14 下午4.49.16.png', '4d12af91-ab84-4e6e-b60d-286fc1d31b3d.png', '2017-03-18 14:42:08', '245872', null), ('109', '20170318测试项目', '400001', '屏幕快照 2016-11-21 下午10.21.10.png', '4813e7ff-9c0b-424d-98bd-ec613771349f.png', '2017-03-18 14:42:08', '278042', null), ('110', '20170318测试项目', '400001', '屏幕快照 2016-12-10 下午12.11.44.png', '38af50d7-6256-4ea4-93cd-f4b08979229f.png', '2017-03-18 14:42:08', '231052', null), ('111', '20170318测试项目', '400001', '屏幕快照 2016-05-14 下午9.48.45.png', '8abe8234-23c0-4a41-ac9a-96433ab21f08.png', '2017-03-18 14:43:44', '405725', null), ('112', '20170318测试项目', '400001', '屏幕快照 2016-05-14 下午9.48.50.png', 'c34cf9e3-c9f7-4a12-8ae1-11a4c57ddb45.png', '2017-03-18 14:43:44', '355908', null), ('113', '20170318测试项目', '400001', '屏幕快照 2016-08-24 上午12.38.36.png', '6b0dd716-6d82-4328-b465-6417d65ad42e.png', '2017-03-18 14:43:44', '693270', null), ('114', '20170318测试项目', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', '3e67c27b-afe4-47de-8898-c7da191af9c4.png', '2017-03-18 14:43:44', '199686', null), ('115', '20170318测试项目', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '9d15a866-6949-4c31-af9b-c418d1d1a556.png', '2017-03-18 14:43:44', '408947', null), ('116', '20170318测试项目', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', 'e2c6e227-8774-45a5-92db-487f69924783.png', '2017-03-18 14:43:44', '331789', null), ('117', '20170318测试项目', '400001', '屏幕快照 2016-11-10 下午4.58.16.png', '6862b8d1-76e5-4170-8c5a-2af14a168be0.png', '2017-03-18 14:43:44', '288201', null), ('118', '20170318测试项目', '400001', '屏幕快照 2016-11-14 下午4.49.16.png', '898e4407-eb71-4359-84c0-8636af1e965d.png', '2017-03-18 14:43:44', '245872', null), ('119', '20170318测试项目', '400001', '屏幕快照 2016-11-21 下午10.21.10.png', '0c128bdb-477f-46d7-af10-b71831ac0447.png', '2017-03-18 14:43:44', '278042', null), ('120', '20170318测试项目', '400001', '屏幕快照 2016-12-10 下午12.11.44.png', '5423d55f-d992-4261-baa9-42ce701a9edd.png', '2017-03-18 14:43:44', '231052', null), ('121', '20170318测试项目', '400001', '屏幕快照 2016-12-14 下午9.56.42.png', 'ccae9f59-6396-4f09-85a2-a91702998a10.png', '2017-03-18 14:43:44', '210998', null), ('122', '20170318测试项目', '400002', '屏幕快照 2016-05-14 下午9.48.50.png', '3db06730-3b3a-4d3c-a962-3d3eb7c2b117.png', '2017-03-18 14:44:07', '355908', null), ('123', '20170318测试项目', '400002', '屏幕快照 2016-08-24 上午12.38.36.png', 'a3e6512f-0e4e-4e49-9832-44095c0e6dde.png', '2017-03-18 14:44:07', '693270', null), ('124', '20170318测试项目', '400002', '屏幕快照 2016-11-07 下午5.19.44.png', '1b3d32bc-b5d2-45d3-bb94-ffa17d7a420b.png', '2017-03-18 14:44:07', '199686', null), ('125', '20170318测试项目', '400002', '屏幕快照 2016-11-09 下午5.39.20.png', 'c51a9ceb-72b8-4ac4-aa7a-7d1fd0f10dc7.png', '2017-03-18 14:44:07', '408947', null), ('126', '20170318测试项目', '400002', '屏幕快照 2016-11-09 下午9.35.47.png', '7e2a4ee7-9dd5-4815-95cb-5fbdf31194a6.png', '2017-03-18 14:44:07', '331789', null), ('127', '20170318测试项目', '400002', '屏幕快照 2016-11-10 下午4.58.16.png', '6b6215f5-3ab5-45aa-b567-4a5d41a00e4d.png', '2017-03-18 14:44:07', '288201', null), ('128', '20170318测试项目', '400002', '屏幕快照 2016-11-14 下午4.49.16.png', 'cf0bf071-3562-44f0-9d1f-eaf757ec683f.png', '2017-03-18 14:44:07', '245872', null), ('129', '20170318测试项目', '400002', '屏幕快照 2016-11-21 下午10.21.10.png', '2b4b6dc9-440c-485a-8b96-daf3f7508898.png', '2017-03-18 14:44:07', '278042', null), ('130', '20170318测试项目', '400002', '屏幕快照 2016-12-10 下午12.11.44.png', 'fa42c5ee-96e5-405b-a9fb-3b5db104a64d.png', '2017-03-18 14:44:07', '231052', null), ('131', '20170318测试项目', '400003', 'QQ截图20170110155124.png', '22ec00c3-1a47-4873-ab23-f940ac1ac236.png', '2017-03-18 14:53:52', '157KB', null), ('132', '20170318测试项目', '400003', 'QQ截图20170110155124.png', '848d4654-5324-48f3-a024-f43686769d9c.png', '2017-03-18 14:54:01', '157KB', null), ('133', '20170318测试项目3', '400001', 'luffy.png', 'b9621766-864d-4f51-8410-8557e6960d8a.png', '2017-03-18 15:21:23', '243855', null), ('134', '20170318测试项目4', '400001', 'luffy.png', 'fa85b616-a3b3-4fdc-8609-d74e0fe9bf4d.png', '2017-03-18 15:55:00', '243855', null), ('135', '20170318测试项目4', '400001', '2424720eec4ecc1a889586731f01536b.jpg', '3b302fff-5cb4-49ee-b33a-9ba7ca0f18cb.jpg', '2017-03-18 15:55:09', '15824', null), ('136', '20170318测试项目4', '400001', '80d455fcbc9b88ecff6c99550bd2f137.jpg', '43cc77a1-6ed3-40d8-9c9b-ca3cc477ecfb.jpg', '2017-03-18 15:55:32', '18268', null), ('137', '20170318测试项目4', '400001', 'QQ截图20151206212109.png', '6c7bcd9b-6534-4cf7-a8c4-1f4c414c01da.png', '2017-03-18 15:55:32', '249318', null), ('138', '20170318测试项目4', '400003', 'luffy.png', 'f5f35564-7423-4dc2-ad68-3e99ec61c923.png', '2017-03-18 16:05:43', '243KB', null), ('139', '20170318测试项目3', '400003', 'luffy.png', '64ab8257-1da8-4dac-ad0b-d0eee8a3ce83.png', '2017-03-18 16:06:44', '243KB', null), ('140', '20170318测试项目2', '400001', '屏幕快照 2016-11-07 下午5.19.44.png', 'b037beef-2bad-4ba3-8b92-7a8a5e7048d6.png', '2017-06-14 14:27:46', '199686', null), ('141', '20170318测试项目2', '400001', '屏幕快照 2016-11-09 下午5.39.20.png', '4ae9b6f3-f785-483e-850a-dbf4115c07ee.png', '2017-06-14 14:27:46', '408947', null), ('142', '20170318测试项目2', '400001', '屏幕快照 2016-11-09 下午9.35.47.png', '306001cf-adff-4140-8d31-c922bab9c8a6.png', '2017-06-14 14:27:46', '331789', null), ('143', '20170318测试项目2', '400001', '屏幕快照 2016-11-21 下午10.21.10.png', 'b09498a4-bf8f-4e55-9638-db44689d7813.png', '2017-06-14 14:27:46', '278042', null), ('144', '20170318测试项目2', '400001', '屏幕快照 2016-12-10 下午12.11.44.png', 'c93ed62b-c182-49a9-a2e4-1939a10e4777.png', '2017-06-14 14:27:46', '231052', null), ('145', '20170318测试项目2', '400001', '屏幕快照 2016-12-15 下午10.42.42.png', '0b439570-f783-459e-9be9-f129a676839e.png', '2017-06-14 14:27:46', '311714', null);
COMMIT;

-- ----------------------------
--  Table structure for `t_project_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_project_base_info`;
CREATE TABLE `t_project_base_info` (
  `projectId` varchar(40) NOT NULL COMMENT '项目编号',
  `projectName` varchar(100) DEFAULT NULL,
  `applicant` varchar(50) DEFAULT NULL,
  `applicationDate` date NOT NULL COMMENT '申请日期',
  `contactPerson` varchar(30) NOT NULL COMMENT '联系人',
  `telephone` varchar(13) NOT NULL COMMENT '联系电话',
  `domain` varchar(16) NOT NULL COMMENT '所属领域',
  `projectFunds` float(7,3) DEFAULT NULL,
  `status` varchar(6) DEFAULT '100001' COMMENT '当前状态',
  `certificate` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否发放证书',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `endDate` date DEFAULT NULL,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目基本信息表\r\n';

-- ----------------------------
--  Records of `t_project_base_info`
-- ----------------------------
BEGIN;
INSERT INTO `t_project_base_info` VALUES ('123', '下午', '太原科技大学', '2017-03-07', '练恒', '18435155334', '现代服务业领域', '48.000', '100008', b'1', null, '2017-03-08'), ('20170316测试项目', '20170316测试项目', '20170316测试项目', '2017-03-16', 'Zychaowill', '18435155316', '能源与节能环保领域', '50.000', '100006', b'0', null, '2017-03-16'), ('20170318测试项目', '20170318测试项目', '太原科技大学', '2017-03-18', 'Zychaowill', '15536542139', '能源与节能环保领域', '50.000', '100010', b'1', null, '2017-03-18'), ('20170318测试项目2', '20170318测试项目2', '科大', '2010-03-23', 'Zychaowill', '18435155316', '能源与节能环保领域', '40.000', '100005', b'0', null, '2016-03-15'), ('20170318测试项目3', '20170318测试项目3', '太原科技大学', '2017-03-18', '张亚超', '15536542139', '能源与节能环保领域', '40.000', '100008', b'0', null, '2017-03-18'), ('20170318测试项目4', '20170318测试项目4', '太原科技大学', '2017-03-18', 'Zychaowill', '15536542139', '能源与节能环保领域', '40.000', '100007', b'0', null, '2017-03-18'), ('2311123', '2016 Ipone销量', '2121', '2016-10-31', '张亚超', '18435155316', '信息领域', '320.000', '100009', b'0', null, null), ('456', '下午1', '太原理工大学', '2017-03-07', '李钊文', '18435155668', '信息领域', '54.000', '100007', b'1', null, '2017-03-08'), ('asdads', 'dasfa', '科大', '2016-10-31', '张亚超', '18435155316', '信息领域', '40.000', '100008', b'0', null, null), ('dasda', 'dsda', '科大', '2016-10-31', '张亚超', '18435155316', '信息领域', '50.000', '100002', b'0', null, null), ('dasdadasda', 'assaasf', 'P1000011', '2016-10-31', 'P100037', '18435155316', '信息领域', '500.000', '100004', b'0', null, null), ('eqweq', 'qw', '科大', '2016-10-31', '张亚超', '18435155316', '信息领域', '40.000', '100004', b'0', null, null), ('P1000011', 'P1000012', '科大', '2016-10-31', '张亚超', '18435155316', '信息领域', '500.000', '100002', b'0', null, null), ('P1000032', 'IPhone销量', '太原科技大学', '2016-10-09', '张亚超', '18435155316', '信息领域', '300.000', '100010', b'1', null, null), ('P100036', 'P102', 'P102', '2016-10-31', '张亚超', '18435155316', '信息领域', '50.000', '100001', b'0', null, null), ('P100037', 'P1000011', '2016 Ipone销量', '2016-10-31', 'Lucene', '18435155316', '信息领域', '500.000', '100001', b'0', null, null), ('P100039', '2016 Iphone 6s销量', 'P101', '2016-10-31', '张亚超', '18435155316', '信息领域', '500.000', '100001', b'0', null, null), ('P100040', '2016 Iphone 6s销量', 'P103', '2016-10-31', 'Lucene', '18435155316', '信息领域', '500.000', '100002', b'0', null, null), ('P1001', '天气好棒', '科技局', '2016-10-31', '18435156027', '18435156027', '能源与节能环保领域', '50.000', '100009', b'0', null, null), ('P1002', '我要出去玩了', '太原科技大学', '2016-10-26', '容嬷嬷', '12245131545', '能源与节能环保领域', '120.000', '100008', b'0', null, null), ('P1003', '迪斯尼有了常', '理工大学', '2016-10-12', '张三丰', '1234567894', '信息领域', '40.000', '100008', b'0', null, null), ('P101', '2017 Iphone6s销量', '科大', '2016-10-31', '张亚超', '18435155316', '信息领域', '40.000', '100004', b'0', null, null), ('ty20161207', '太原市科技局项目', '2016.11.22', '2016-12-07', '张亚超', '18435155316', '交通运输领域', '40.000', '100009', b'0', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `t_role_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_role_base_info`;
CREATE TABLE `t_role_base_info` (
  `roleId` varchar(40) NOT NULL COMMENT '编号',
  `roleName` varchar(50) DEFAULT NULL,
  `isUse` bit(1) DEFAULT b'1',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基本信息表';

-- ----------------------------
--  Records of `t_role_base_info`
-- ----------------------------
BEGIN;
INSERT INTO `t_role_base_info` VALUES ('09c7eff4-7a93-11e6-a8a9-206a8ab73904', '会议主持人', b'1', '会议主持人员', '会议主持人员'), ('2c604a17-7a93-11e6-a8a9-206a8ab73904', '系统管理员', b'1', '系统管理员', '系统管理员'), ('c49b4a97-7a92-11e6-a8a9-206a8ab73904', '项目验收资料登记人', b'1', '项目验收资料登记人', '项目验收资料登记人'), ('dd868784-7a92-11e6-a8a9-206a8ab73904', '项目验收负责人', b'1', '项目验收负责人', '项目验收负责人'), ('f3a432bb-7a92-11e6-a8a9-206a8ab73904', '会议创建人', b'1', '会议创建人员', '会议创建人员'), ('f3a432bb-7a92-11e6-a8a9-206a8ab739e7', '证书发放人', b'1', '证书发放人', '证书发放人');
COMMIT;

-- ----------------------------
--  Table structure for `t_templeate_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_templeate_base_info`;
CREATE TABLE `t_templeate_base_info` (
  `templateId` varchar(40) NOT NULL COMMENT '模板编号',
  `templateName` varchar(10) NOT NULL COMMENT '模板名称',
  `templateURL` longtext NOT NULL COMMENT '内容(url)',
  `remark` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`templateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `t_unit_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_unit_base_info`;
CREATE TABLE `t_unit_base_info` (
  `unitId` varchar(40) NOT NULL COMMENT '部门编号',
  `unitName` varchar(30) NOT NULL COMMENT '名称',
  `upUnitId` varchar(40) DEFAULT NULL COMMENT '上级部门编号',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`unitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='部门信息表';

-- ----------------------------
--  Records of `t_unit_base_info`
-- ----------------------------
BEGIN;
INSERT INTO `t_unit_base_info` VALUES ('10', '科技评估中心', '', ''), ('10001', '主任办公室', '10', '主任办公室'), ('10002', '项目评估部', '10', '项目评估部'), ('10003', '成果评价部', '10', '成果评价部');
COMMIT;

-- ----------------------------
--  Table structure for `t_user_account`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_account`;
CREATE TABLE `t_user_account` (
  `uaId` varchar(40) NOT NULL,
  `uId` varchar(40) NOT NULL,
  `account` varchar(30) NOT NULL,
  `password` varchar(16) NOT NULL,
  PRIMARY KEY (`uaId`),
  KEY `FK_UC_UID` (`uId`),
  CONSTRAINT `FK_UC_UID` FOREIGN KEY (`uId`) REFERENCES `t_user_base_info` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `t_user_account`
-- ----------------------------
BEGIN;
INSERT INTO `t_user_account` VALUES ('738e9ece-89f7-11e6-81ea-208984976338', 'U100002', '项目登记人', '123456'), ('7df4af79-89f7-11e6-81ea-208984976338', 'U100003', '项目负责人', '123456'), ('8487ebdf-89f7-11e6-81ea-208984976338', 'U100004', '会议创建人', '123456'), ('89fad9af-89f7-11e6-81ea-208984976338', 'U100005', '会议主持人', '123456'), ('900d1b02-89f7-11e6-81ea-208984976338', 'U100006', '系统管理员', '123456'), ('9628a231-89f7-11e6-81ea-208984976338', 'U100001', 'root', '123456'), ('a3cf55ba-0575-11e7-98aa-076d0efed018', 'U100007', '会议主持人2', '123456');
COMMIT;

-- ----------------------------
--  Table structure for `t_user_base_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_base_info`;
CREATE TABLE `t_user_base_info` (
  `userId` varchar(40) NOT NULL COMMENT '编号',
  `unitId` varchar(40) NOT NULL COMMENT '部门编号',
  `userName` varchar(30) DEFAULT NULL COMMENT '用户名',
  `name` varchar(30) NOT NULL,
  `sex` varchar(6) NOT NULL COMMENT '性别',
  `password` varchar(16) NOT NULL COMMENT '密码',
  `duty` varchar(10) NOT NULL COMMENT '职位',
  `telephone` varchar(13) NOT NULL,
  PRIMARY KEY (`userId`),
  KEY `FK_Reference_32` (`unitId`),
  CONSTRAINT `FK_Reference_32` FOREIGN KEY (`unitId`) REFERENCES `t_unit_base_info` (`unitId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
--  Records of `t_user_base_info`
-- ----------------------------
BEGIN;
INSERT INTO `t_user_base_info` VALUES ('U100001', '10001', null, 'admin', '男', '123456', '高级管理员', '18435155316'), ('U100002', '10001', null, '项目验收登记人员', '男', '123456', '项目验收登记人员', '18435155227'), ('U100003', '10001', null, '项目验收负责人', '男', '123456', '项目验收负责人', '18435156270'), ('U100004', '10001', null, '会议创建人员', '男', '123456', '会议创建人员', '18435155317'), ('U100005', '10001', null, '会议主持人', '男', '123456', '会议主持人', '18435155318'), ('U100006', '10001', null, '系统管理员', '男', '123456', '系统管理员', '18435155319'), ('U100007', '10001', null, '会议主持人2', '男', '123456', '会议主持人2', '15536542139');
COMMIT;

-- ----------------------------
--  Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `userId` varchar(40) NOT NULL COMMENT '编号',
  `roleId` varchar(40) NOT NULL COMMENT '编号',
  PRIMARY KEY (`userId`,`roleId`),
  KEY `FK_Reference_36` (`roleId`),
  CONSTRAINT `FK_Reference_36` FOREIGN KEY (`roleId`) REFERENCES `t_role_base_info` (`roleId`),
  CONSTRAINT `FK_USER` FOREIGN KEY (`userId`) REFERENCES `t_user_base_info` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
--  Records of `user_role`
-- ----------------------------
BEGIN;
INSERT INTO `user_role` VALUES ('U100001', '09c7eff4-7a93-11e6-a8a9-206a8ab73904'), ('U100005', '09c7eff4-7a93-11e6-a8a9-206a8ab73904'), ('U100007', '09c7eff4-7a93-11e6-a8a9-206a8ab73904'), ('U100001', '2c604a17-7a93-11e6-a8a9-206a8ab73904'), ('U100006', '2c604a17-7a93-11e6-a8a9-206a8ab73904'), ('U100001', 'c49b4a97-7a92-11e6-a8a9-206a8ab73904'), ('U100002', 'c49b4a97-7a92-11e6-a8a9-206a8ab73904'), ('U100001', 'dd868784-7a92-11e6-a8a9-206a8ab73904'), ('U100003', 'dd868784-7a92-11e6-a8a9-206a8ab73904'), ('U100001', 'f3a432bb-7a92-11e6-a8a9-206a8ab73904'), ('U100004', 'f3a432bb-7a92-11e6-a8a9-206a8ab73904'), ('U100001', 'f3a432bb-7a92-11e6-a8a9-206a8ab739e7');
COMMIT;

-- ----------------------------
--  View structure for `v_complete_material_file`
-- ----------------------------
DROP VIEW IF EXISTS `v_complete_material_file`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_complete_material_file` AS select `complete_material`.`fileId` AS `fileId`,`complete_material`.`completeId` AS `completeId`,`complete_material`.`projectId` AS `projectId`,`t_files_index`.`fileType` AS `fileType`,`t_files_index`.`originalName` AS `originalName`,`t_files_index`.`currentName` AS `currentName`,`t_files_index`.`uploadTime` AS `uploadTime`,`t_files_index`.`fileSize` AS `fileSize`,`t_files_index`.`remark` AS `remark` from (`complete_material` join `t_files_index` on((`complete_material`.`fileId` = `t_files_index`.`fileId`)));

-- ----------------------------
--  View structure for `v_project_accept_info`
-- ----------------------------
DROP VIEW IF EXISTS `v_project_accept_info`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_project_accept_info` AS select `tpbi`.`projectId` AS `projectId`,`tpbi`.`projectName` AS `projectName`,`tpbi`.`applicant` AS `applicant`,`tpbi`.`applicationDate` AS `applicationDate`,`tpbi`.`contactPerson` AS `contactPerson`,`tpbi`.`telephone` AS `telephone`,`tpbi`.`domain` AS `domain`,`tpbi`.`projectFunds` AS `projectFunds`,`tpbi`.`status` AS `status`,`tpbi`.`certificate` AS `certificate`,`tpbi`.`remark` AS `remark`,`pa`.`recordDate` AS `recordDate`,`pa`.`recordPerson` AS `recordPerson`,`pa`.`chargePerson` AS `chargePerson`,`pa`.`chargeUnit` AS `chargeUnit`,`pa`.`distributionDate` AS `distributionDate`,`pa`.`receiveDate` AS `receiveDate`,`pa`.`receivePerson` AS `receivePerson`,`pa`.`receiveResult` AS `receiveResult`,`pa`.`remark1` AS `remark1`,`pa`.`remark2` AS `remark2`,`pa`.`remark3` AS `remark3`,`pc`.`censorDate` AS `censorDate`,`pc`.`censorPerson` AS `censorPerson`,`pc`.`censorResult` AS `censorResult`,`ei`.`inspectPerson` AS `inspectPerson`,`ei`.`inspectDate` AS `inspectDate`,`ei`.`inspectResult` AS `inspectResult`,`ei`.`enterpriseReceiver` AS `enterpriseReceiver`,`ei`.`isAudit` AS `isAudit`,`ia`.`financeUnit` AS `financeUnit`,`ia`.`auditingPerson` AS `auditingPerson`,`ia`.`auditDate` AS `auditDate`,`ia`.`enterpriseReceiver2` AS `enterpriseReceiver2` from ((((`t_project_base_info` `tpbi` left join `project_accept` `pa` on((`tpbi`.`projectId` = `pa`.`projectId`))) left join `project_censor` `pc` on((`tpbi`.`projectId` = `pc`.`projectId`))) left join `enterprise_inspect` `ei` on((`tpbi`.`projectId` = `ei`.`projectId`))) left join `inspect_audit` `ia` on((`ei`.`inspectId` = `ia`.`inspectId`)));

-- ----------------------------
--  View structure for `v_project_material_file`
-- ----------------------------
DROP VIEW IF EXISTS `v_project_material_file`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_project_material_file` AS select `project_material`.`fileId` AS `fileId`,`project_material`.`materialId` AS `materialId`,`project_material`.`projectId` AS `projectId`,`t_files_index`.`fileType` AS `fileType`,`t_files_index`.`originalName` AS `originalName`,`t_files_index`.`currentName` AS `currentName`,`t_files_index`.`uploadTime` AS `uploadTime`,`t_files_index`.`fileSize` AS `fileSize`,`t_files_index`.`remark` AS `remark`,`t_files_index`.`opinionContent` AS `opinionContent` from (`project_material` join `t_files_index` on((`project_material`.`fileId` = `t_files_index`.`fileId`)));

-- ----------------------------
--  View structure for `v_project_meeting_notify`
-- ----------------------------
DROP VIEW IF EXISTS `v_project_meeting_notify`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_project_meeting_notify` AS select `t_project_base_info`.`projectId` AS `projectId`,`t_project_base_info`.`projectName` AS `projectName`,`t_project_base_info`.`applicant` AS `applicant`,`t_project_base_info`.`applicationDate` AS `applicationDate`,`t_project_base_info`.`contactPerson` AS `contactPerson`,`t_project_base_info`.`telephone` AS `telephone`,`t_project_base_info`.`domain` AS `domain`,`t_project_base_info`.`projectFunds` AS `projectFunds`,`t_project_base_info`.`status` AS `status`,`t_project_base_info`.`certificate` AS `certificate`,`t_project_base_info`.`remark` AS `remark`,`project_meeting`.`meetingId` AS `meetingId`,`project_meeting`.`notifyDate` AS `notifyDate`,`project_meeting`.`notifyMethod` AS `notifyMethod` from (`t_project_base_info` join `project_meeting` on((`t_project_base_info`.`projectId` = `project_meeting`.`projectId`)));

SET FOREIGN_KEY_CHECKS = 1;
